#!/bin/bash
### BEGIN INIT INFO
# Provides:          ax25spyd
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start ax25spyd at boot
### END INIT INFO
#
# skeleton	example file to build /etc/init.d/ scripts.
#		This file should be used to construct scripts for /etc/init.d.
#
#		Written by Miquel van Smoorenburg <miquels@cistron.nl>.
#		Modified for Debian GNU/Linux
#		by Ian Murdock <imurdock@gnu.ai.mit.edu>.
#
# Version:	@(#)skeleton  1.8  03-Mar-1998  miquels@cistron.nl
#
# This file was automatically customized by dh-make on Sun, 28 Apr 2002 16:27:48 +0200

PATH=/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/sbin/ax25spyd
NAME=ax25spyd
DESC=ax25spyd

. /lib/lsb/init-functions

test -f $DAEMON || exit 0

set -e

case "$1" in
  start)
	echo -n "Starting $DESC: "
	start-stop-daemon --start --quiet -o --exec $DAEMON 2&>/dev/null 
	echo "$NAME."
	;;
  stop)
	echo -n "Stopping $DESC: "
	start-stop-daemon --stop --quiet -o --exec $DAEMON 
	echo "$NAME."
	;;
  restart|force-reload)
	echo -n "Restarting $DESC: "
	start-stop-daemon --stop --quiet -o --exec $DAEMON 
	sleep 1
	start-stop-daemon --start --quiet -o --exec $DAEMON 2&>/dev/null 
	echo "$NAME."
	;;
  *)
	N=/etc/init.d/$NAME
	echo "Usage: $N {start|stop|restart|force-reload}" >&2
	exit 1
	;;
esac

exit 0
