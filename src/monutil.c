/* monutil.c
 * contains functions to be used by the client and by the server
 *  13.02.99 kw created
 * $Id: monutil.c,v 1.18 1999/06/09 19:33:50 walter Exp $
 */

#include <stdio.h>
#include <stdarg.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>

#include "mondefs.h"
#include "monutil.h"


char*
pid2str(int pid)
{
    static char str[20];
    switch( pid ) {
	case PID_NO_L3:    return "Text";
	case PID_VJIP06:   return "IP-VJ.6";
	case PID_VJIP07:   return "IP-VJ.7";
	case PID_SEGMENT:  return "Segment";
	case PID_IP:	   return "IP";
	case PID_ARP:	   return "ARP";
	case PID_NETROM:   return "NET/ROM";
	case PID_X25:	   return "X.25";
	case PID_TEXNET:   return "Texnet";
	case PID_FLEXNET:  return "Flexnet";
	case PID_PSATFT:   return "PacSat FT";
	case PID_PSATPB:   return "PacSat PB";
	case -1: /*no PID*/return "";
	default:	   sprintf(str,"pid=0x%x",pid);
			   return str;
    }
}

int
frametype2ord(int type)
{
	switch (type) {
		case I:      return ORD_I;
		case SABM:   return ORD_SABM;
		case SABME:  return ORD_SABME;
		case DISC:   return ORD_DISC;
		case DM:     return ORD_DM;
		case UA:     return ORD_UA;
		case RR:     return ORD_RR;
		case RNR:    return ORD_RNR;
		case REJ:    return ORD_REJ;
		case FRMR:   return ORD_FRMR;
		case UI:     return ORD_UI;
		default:     return ORD_OTHER;
	}
}


char*
ord2frametypestring(int ordtype)
{
    switch (ordtype) {
	case ORD_I     :    return "I";
	case ORD_SABM  :    return "C";
	case ORD_SABME :    return "CE";
	case ORD_DISC  :    return "D";
	case ORD_DM    :    return "DM";
	case ORD_UA    :    return "UA";
	case ORD_RR    :    return "RR";
	case ORD_RNR   :    return "RNR";
	case ORD_REJ   :    return "REJ";
	case ORD_FRMR  :    return "FRMR";
	case ORD_UI    :    return "UI";
	default:	    return "[invalid]";
    }
}

int
get16(unsigned char *cp)
{
	int x;

	x  = *cp++;   x <<= 8;
	x |= *cp++;

	return(x);
}

int
get32(unsigned char *cp)
{
	int x;

	x  = *cp++;  x <<= 8;
	x |= *cp++;  x <<= 8;
	x |= *cp++;  x <<= 8;
	x |= *cp;

	return(x);
}


char*
servname(int port, char *proto)
/* Gib den Servicenamen f�r port zur�ck. Falls der nicht existiert
 * die Nummer als 16Byte StringBuf */
{
	struct servent *serv;
	static char str[16]; /* ! */

	if ((serv = getservbyport(htons(port), proto)))
		strncpy(str, serv->s_name, 16);
	else
		snprintf(str, 16, "%i", port);

	return str;
}

/*----------------------------------------------------------------------*/

char*
ax25call2bufEnd(struct t_ax25call *pax25call, char *buf)
/* same as ax25call2str(), but caller have to provide own buffer
 * returns pointer to END of used buffer */
{
    if( pax25call->ssid == 0)
	 return( buf+sprintf(buf,"%s",   pax25call->sCall                 ) );
    else
	 return( buf+sprintf(buf,"%s-%d",pax25call->sCall, pax25call->ssid) );
}

char*
ax25call2strBuf(struct t_ax25call *pax25call, char *buf)
{
    ax25call2bufEnd(pax25call,buf);
    return buf;
}

char*
ax25call2str(struct t_ax25call *pax25call)
{
    static char res[9+1]; /* fixme */
    ax25call2bufEnd(pax25call,res);
    return res;
}

/*----------------------------------------------------------------------*/

char*
time2str(time_t *ptime)
{
	struct tm *ptm_now;
	static char tmstring[40];

	ptm_now = localtime(ptime);
	strftime(tmstring, sizeof(tmstring)-1,
		 "%d.%m.%y %H:%M:%S"
		 /* "[%c]\n" */
		 ,ptm_now );
	return( tmstring );
}

ssize_t
readfull( int fd, void *buf, size_t count )
/* same as read(2), but will always wait until count bytes are read or error
 * occured */
{
    int size = 0;
    int res = 0;
    int nLoop = 0;

    while( size < count ) {
	/* if( nLoop == 1 )
	 *   syslog(LOG_DEBUG, "readfull-loop: %d", nLoop);
	 */
	if( nLoop == 100 ) {
	    syslog(LOG_ERR, "aborting: readfull-loop: %d", nLoop);
	    /* exit(1); */
	}
	res = read(fd, buf+size, count-size);
	if( res <= 0 ) { return res; }
	size += res;
	nLoop++;
    }
    return size;
}


void
convertBufferToReadable( char *p, int size )
{
    int i;

    for ( i = 0; i < size; p++, i++)
	    if( *p == '\x0d' )
		    *p = '\n';
	    else if( ( *p < 32 && *p >= 0 && *p != '\n') )
		    *p = '.';
}


