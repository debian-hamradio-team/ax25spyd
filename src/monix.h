/*
 *  ax25spy - a client for ax25spyd
 *
 *  Copyright (C) 1999 Free Software Foundation, Inc.
 *  Copyright (C) 1999 Walter Koch, dg9ep
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
 * $Id: monix.h,v 1.13 1999/04/25 18:08:32 walter Exp $
 * $Revision: 1.13 $
 * $State: Exp $
 * $Name:  $
 */


#include "mondefs.h"
#include "monutil.h"


/* In utils.c */
extern int color;                       /* Colorized mode */
extern int sevenbit;                    /* Are we on a 7-bit terminal? */
extern int ibmhack;                     /* IBM mapping? */

void tcprint(int dtype, char *str, int size);
void lprintf(int dtype, char *fmt, ...);
int  initcolor(void);
int write_string(int *fd,const char *str);

void data_dump(unsigned char *, int, int);
int  get16(unsigned char *);
int  get32(unsigned char *);

