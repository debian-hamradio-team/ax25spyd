/***************************************
  monix, dg9ep, GPL'ed

  $Id: sockets.h,v 1.5 1999/06/09 19:33:51 walter Exp $

  copied from:

  WWWOFFLE - World Wide Web Offline Explorer - Version 2.1b.
  Socket function header file.
  ******************/ /******************
  Written by Andrew M. Bishop

  This file Copyright 1996,97,98 Andrew M. Bishop
  It may be distributed under the GNU Public License, version 2, or
  any higher version.  See section COPYING of the GNU Public license
  for conditions under which this file may be redistributed.
  ***************************************/


#ifndef SOCKETS_H
#define SOCKETS_H    /*+ To stop multiple inclusions. +*/

/* */
/* #define AF_MY AF_UNIX */
#define AF_MY AF_INET


/* in sockets.c */

int OpenClientSocket(char* host, int port);

int OpenServerSocket(int port);

int SocketRemoteName(int socket,char **name,char **ipname,int *port);
int SocketLocalName(int socket,char **name,char **ipname,int *port);

int CloseSocket(int socket);

char *GetFQDN(void);


#endif /* SOCKETS_H */
