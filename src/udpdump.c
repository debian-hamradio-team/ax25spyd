/* UDP packet tracing
 * Copyright 1991 Phil Karn, KA9Q
 * used for ax25spyd. dg9ep, 1999
 * $Id: udpdump.c,v 1.7 1999/05/15 20:20:47 walter Exp $
 */

#include <stdio.h>
#include "monixd.h"

#define RIP_PORT	520
#define DNS_PORT	53

#define UDPHDR		8

/* Dump a UDP header */
void
udp_dump(unsigned char *data, int length, int dumpstyle)
{
	int hdr_length;
	int source;
	int dest;

	mheard.protUDP++;

	hdr_length = get16(data + 4);
	source	   = get16(data + 0);
	dest	   = get16(data + 2);

	lprintf(T_PROTOCOL, "UDP: ");

	lprintf(T_ADDR, "%s->%s", servname(source, "udp"),
				    servname(dest,   "udp"));
	lprintf(T_TCPHDR, " len:", hdr_length);
	lprintf(T_HDRVAL, "%d ",    hdr_length);

	if (hdr_length > UDPHDR) {
		length -= UDPHDR;
		data   += UDPHDR;

		switch (dest) {
			case RIP_PORT:
				lprintf(T_TCPHDR, "\n");
				rip_dump(data, length);
				break;
			case DNS_PORT:
				lprintf(T_TCPHDR, "\n");
				dns_dump(data, length);
				break;
			default:
				lprintf(T_TCPHDR, " Data:");
				lprintf(T_HDRVAL, "%d\n", length);
				data_dump(data, length, ASCII);
				break;
		}
	}
}

