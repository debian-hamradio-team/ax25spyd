/*
 *  ax25spyd - An AX.25 monitorspy daemon
 *
 *  Copyright (C) 1999 Free Software Foundation, Inc.
 *  Copyright (C) 1999 Walter Koch, dg9ep
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* monix's version of  WWWOFFLE's Socket manipulation routines.
 * Originaly written by Andrew M. Bishop
 * This file Copyright 1996,97,98 Andrew M. Bishop
 */


#include <stdio.h>
#include <string.h>

#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>

#include <netdb.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "sockets.h"

int
OpenClientSocket(char* host, int port)
/* Opens a socket for a client.
 * int OpenClientSocket Returns the socket file descriptor.
 * char* host The name of the remote host.
 * int port The socket number. */
{
    int s;
    int retval,err=0;
    struct sockaddr_in server;
    struct hostent* hp;
    int retries=1;

    server.sin_family=AF_MY;
    server.sin_port=htons((unsigned short)port);

    hp = gethostbyname(host);
    if(!hp) {
       unsigned long int addr=inet_addr(host);

       if(addr!=-1)
	  hp = gethostbyaddr((char*)addr,sizeof(addr),AF_MY);
       if(!hp) {
	   errno=-1;
	   printf("Warning: Unknown host '%s' for server.",host);
	   return(-1);
       }
    }
    memcpy((char*)&server.sin_addr,(char*)hp->h_addr,sizeof(server.sin_addr));

    do {
       s = socket(PF_INET,SOCK_STREAM,0);
       if(s==-1) {
	   printf("Cannot create client socket .");
	   return(-1);
       }

       retval = connect(s,(struct sockaddr *)&server,sizeof(server));
       if(retval==-1) {
	  err = errno;
	  if(--retries && errno==ECONNREFUSED)
	     printf("Inform, connect fail ; trying again.");
	  else
	     printf("Warning, connect fail.");
	  close(s);
	  s = -1 ;
	  sleep(1);
	}
    } while(retval==-1 && retries && err==ECONNREFUSED);

    return(s);
}


/*++++++++++++++++++++++++++++++++++++++
  Opens a socket for a server.
  Returns a socket to be a server.
  int port - The port number to use.
  ++++++++++++++++++++++++++++++++++++++*/
int
OpenServerSocket(int port)
{
    int s;
    int retval;
    struct sockaddr_in server;
    int reuse_addr=1;

    s = socket(PF_INET,SOCK_STREAM,0);
    if(s==-1) {
      printf("Cannot create server socket .");return(-1);}

    setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&reuse_addr,sizeof(reuse_addr));

    server.sin_family	   = AF_MY;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port	   = htons((unsigned int)port);

    retval = bind(s,(struct sockaddr*)&server,sizeof(server));
    if(retval==-1)
      {printf("failed to bind server socket \n");return(-1);}

    listen(s,8);

    return(s);
}


/*++++++++++++++++++++++++++++++++++++++
  Determines the hostname and port number used for a socket on the other end.
  int SocketRemoteName Returns 0 on success and -1 on failure.
  int socket Specifies the socket to check.
  char **name Returns the hostname.
  char **ipname Returns the hostname as an IP address.
  int *port Returns the port number.
  ++++++++++++++++++++++++++++++++++++++*/
int
SocketRemoteName(int socket,char **name,char **ipname,int *port)
{
    struct sockaddr_in server;
    int length=sizeof(server),retval;
    static char host[MAXHOSTNAMELEN],ip[16];
    struct hostent* hp=NULL;

    retval=getpeername(socket,(struct sockaddr*)&server,&length);
    if(retval==-1)
       printf("failed to get socket peername");
    else
      {
       hp=gethostbyaddr((char*)&server.sin_addr,sizeof(server.sin_addr),AF_MY);
       if(hp)
	  strcpy(host,hp->h_name);
       else
	  strcpy(host,inet_ntoa(server.sin_addr));

       strcpy(ip,inet_ntoa(server.sin_addr));

       if(name)
	  *name=host;
#ifdef __CYGWIN__
       if(!strcmp(ip,"127.0.0.1"))
	  *name="localhost";
#endif
       if(ipname)
	  *ipname=ip;
       if(port)
	  *port=ntohs(server.sin_port);
      }

    return(retval);
}


/*++++++++++++++++++++++++++++++++++++++
  Determines the hostname and port number used for a socket on this end.
  int SocketLocalName Returns 0 on success and -1 on failure.
  int socket Specifies the socket to check.
  char **name Returns the hostname.
  char **ipname Returns the hostname as an IP address.
  int *port Returns the port number.
  ++++++++++++++++++++++++++++++++++++++*/
int
SocketLocalName(int socket,char **name,char **ipname,int *port)
{
    struct sockaddr_in server;
    int length=sizeof(server),retval;
    static char host[MAXHOSTNAMELEN],ip[16];
    struct hostent* hp=NULL;

    retval=getsockname(socket,(struct sockaddr*)&server,&length);
    if(retval==-1)
       printf("failed to get socket name ");
    else
      {
       hp=gethostbyaddr((char*)&server.sin_addr,sizeof(server.sin_addr),AF_MY);
       if(hp)
	  strcpy(host,hp->h_name);
       else
	  strcpy(host,inet_ntoa(server.sin_addr));

       strcpy(ip,inet_ntoa(server.sin_addr));

       if(name)
	  *name=host;
#ifdef __CYGWIN__
       if(!strcmp(ip,"127.0.0.1"))
	  *name="localhost";
#endif
       if(ipname)
	  *ipname=ip;
       if(port)
	  *port=ntohs(server.sin_port);
      }

    return(retval);
}


/*++++++++++++++++++++++++++++++++++++++
  Closes a previously opened socket.
  int CloseSocket Returns 0 on success, -1 on error.
  int socket The socket to close
  ++++++++++++++++++++++++++++++++++++++*/
int
CloseSocket(int socket)
{
    int retval=close(socket);

    if(retval==-1)
       printf("socket close failed ");

    return(retval);
}


/*++++++++++++++++++++++++++++++++++++++
  Get the Fully Qualified Domain Name for the local host.
  char *GetFQDN Return the FQDN.
  ++++++++++++++++++++++++++++++++++++++*/
char*
GetFQDN(void)
{
    static char fqdn[256],**h;
    struct hostent* hp;
    int type=hp->h_addrtype;
    int length=hp->h_length;
    char addr[sizeof(struct in_addr)];

    /* Try gethostname(). */
    if(gethostname(fqdn,255)==-1)
      {printf("failed to get hostname.");return(NULL);}

    if(strchr(fqdn,'.'))
       return(fqdn);

    /* Try gethostbyname(). */
    hp=gethostbyname(fqdn);
    if(!hp) {
	errno=-1;
	printf("Can't get IP address for host .");
	return(NULL);
    }

    if(strchr(hp->h_name,'.')) {
       strcpy(fqdn,hp->h_name);
       return(fqdn);
    }

    for(h=hp->h_aliases;*h;h++)
       if(strchr(*h,'.')) {
	  strcpy(fqdn,*h);
	  return(fqdn);
       }

    /* Try gethostbyaddr(). */

    type=hp->h_addrtype;
    length=hp->h_length;
    memcpy(addr,(char*)hp->h_addr,sizeof(struct in_addr));

    hp=gethostbyaddr(addr,length,type);
    if(!hp) {
	errno=-1;
	printf("Can't get hostname for IP address .");
	return(NULL);
    }

    if(strchr(hp->h_name,'.')) {
       strcpy(fqdn,hp->h_name);
       return(fqdn);
    }

    for(h=hp->h_aliases;*h;h++)
       if(strchr(*h,'.')) {
	  strcpy(fqdn,*h);
	  return(fqdn);
       }

    strcpy(fqdn,hp->h_name);

    return(fqdn);
}



/* from buildsaddr.c */
struct sockaddr *
build_sockaddr(const char *name, int *addrlen)
{ char *host_name;
  char *serv_name;
  char buf[1024];
  static union {
    struct sockaddr sa;
    struct sockaddr_in si;
    struct sockaddr_un su;
  } addr;

  memset((char *) &addr, 0, sizeof(addr));
  *addrlen=0;

  host_name=strcpy(buf, name);
  serv_name=strchr(buf, ':');
  if(!serv_name)
    return 0;
  *serv_name++=0;
  if(!*host_name || !*serv_name)
    return 0;

  if(!strcmp(host_name, "local") || !strcmp(host_name, "unix")) {
    addr.su.sun_family=AF_UNIX;
    *addr.su.sun_path=0;
    strcat(addr.su.sun_path, serv_name);
    *addrlen=sizeof(struct sockaddr_un);
    return &addr.sa;
  }

  addr.si.sin_family=AF_INET;

  if(!strcmp(host_name, "*")) {
    addr.si.sin_addr.s_addr=INADDR_ANY;
  } else if(!strcmp(host_name, "loopback")) {
    addr.si.sin_addr.s_addr=inet_addr("127.0.0.1");
  } else if((addr.si.sin_addr.s_addr=inet_addr(host_name))==-1L) {
    struct hostent *hp=gethostbyname(host_name);
    endhostent();
    if(!hp)
      return 0;
    addr.si.sin_addr.s_addr=((struct in_addr *) (hp->h_addr))->s_addr;
  }

  if(isdigit(*serv_name & 0xff)) {
    addr.si.sin_port=htons(atoi(serv_name));
  } else {
    struct servent *sp=getservbyname(serv_name, (char *) 0);
    endservent();
    if(!sp)
      return 0;
    addr.si.sin_port=sp->s_port;
  }

  *addrlen=sizeof(struct sockaddr_in);
  return &addr.sa;
}


