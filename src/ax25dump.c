/* ax25dump.c
 * AX25 header tracing
 * Copyright 1991 Phil Karn, KA9Q
 * Extended for listen() by ???
 * 07.02.99 Changed for monixd by dg9ep
 *
 * $Id: ax25dump.c,v 1.18 1999/06/28 22:17:34 walter Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <syslog.h>

#include "monixd.h"

#define LAPB_UNKNOWN	0
#define LAPB_COMMAND	1
#define LAPB_RESPONSE	2

#define SEG_FIRST	0x80
#define SEG_REM 	0x7F


/* unsigned char typeconv[14] =
 * { S, RR, RNR, REJ, U,
 *  SABM, SABME, DISC, DM, UA,
 *  FRMR, UI, PF, EPF
 * };
 */


#define MMASK		7

#define HDLCAEB 	0x01
#define SSID		0x1E
#define REPEATED	0x80
#define C		0x80
#define SSSID_SPARE	0x40
#define ESSID_SPARE	0x20

#define ALEN		6
#define AXLEN		7

#define W		1
#define X		2
#define Y		4
#define Z		8

static int  ftype(unsigned char *, int *, int *, int *, int *, int);
static char *decode_type(int);
int mem2ax25call(struct t_ax25call*, unsigned char*);

#define NDAMA_STRING ""
#define DAMA_STRING "[DAMA] "

/*----------------------------------------------------------------------*/

/* FlexNet header compression display by Thomas Sailer sailer@ife.ee.ethz.ch */
/* Dump an AX.25 packet header */
struct t_ax25packet *
ax25_decode( struct t_ax25packet *ax25packet , unsigned char *data, int length)
{
	int ctlen;
	int eoa;

	ax25packet->pid = -1;

	ax25packet->totlength = length;
	mheard.nTotalBytes+=length;

	/* check for FlexNet compressed header first; FlexNet header
	 * compressed packets are at least 8 bytes long  */
	if (length < 8) {
		/* Something wrong with the header */
		return ax25packet;
	}
	if (data[1] & HDLCAEB) {
		/* char tmp[15]; */
		/* this is a FlexNet compressed header	$TODO */
/*		 tmp[6] = tmp[7] = ax25packet->fExtseq = 0;
 *		 tmp[0] = ' ' + (data[2] >> 2);
 *		 tmp[1] = ' ' + ((data[2] << 4) & 0x30) + (data[3] >> 4);
 *		 tmp[2] = ' ' + ((data[3] << 2) & 0x3c) + (data[4] >> 6);
 *		 tmp[3] = ' ' + (data[4] & 0x3f);
 *		 tmp[4] = ' ' + (data[5] >> 2);
 *		 tmp[5] = ' ' + ((data[5] << 4) & 0x30) + (data[6] >> 4);
 *		 if (data[6] & 0xf)
 *			 sprintf(tmp+7, "-%d", data[6] & 0xf);
 *		 lprintf(T_ADDR, "%d->%s%s",
 *			 (data[0] << 6) | ((data[1] >> 2) & 0x3f),
 *			 strtok(tmp, " "), tmp+7);
 *		 ax25packet->cmdrsp = (data[1] & 2) ? LAPB_COMMAND : LAPB_RESPONSE;
 *		 dama = NDAMA_STRING;
 *		 data	+= 7;
 *		 length -= 7;
 */
		 return ax25packet; /* $TODO */
	} else {
		/* Extract the address header */
		if (length < (AXLEN + AXLEN + 1)) {
			/* length less then fmcall+tocall+ctlbyte */
			/* Something wrong with the header */
			return ax25packet;
		}

		ax25packet->fExtseq = (
			 (data[AXLEN + ALEN] & SSSID_SPARE) != SSSID_SPARE
		);

		ax25packet->fDama = ((data[AXLEN + ALEN] & ESSID_SPARE) != ESSID_SPARE);
		if(ax25packet->fDama) {
			mheard.damaframes++;
		}

		mem2ax25call( &ax25packet->tocall, data 	);
		mem2ax25call( &ax25packet->fmcall, data + AXLEN );

		ax25packet->cmdrsp = LAPB_UNKNOWN;
		if ((data[ALEN] & C) && !(data[AXLEN + ALEN] & C))
			ax25packet->cmdrsp = LAPB_COMMAND;

		if ((data[AXLEN + ALEN] & C) && !(data[ALEN] & C))
			ax25packet->cmdrsp = LAPB_RESPONSE;

		eoa = (data[AXLEN + ALEN] & HDLCAEB);

		data   += (AXLEN + AXLEN);
		length -= (AXLEN + AXLEN);

		ax25packet->ndigi = 0;
		ax25packet->ndigirepeated = -1;
		if (!eoa) {
			while (!eoa) {
				mem2ax25call( &ax25packet->digi[ax25packet->ndigi], data );
				if( data[ALEN] & REPEATED )
				       ax25packet->ndigirepeated = ax25packet->ndigi;

				eoa = (data[ALEN] & HDLCAEB);
				ax25packet->ndigi++;
				data   += AXLEN;
				length -= AXLEN;
			}
		}
	}

	ax25packet->fFmCallIsLower = (
	    memcmp( &ax25packet->fmcall,
		    &ax25packet->tocall, sizeof(struct t_ax25call)
		  ) < 0
	);

	ax25packet->pdata = data;
	ax25packet->datalength = length;

	if (length == 0) {
	    ax25packet->valid = 1;
	    return ax25packet;
	}

	ctlen = ftype(data, &ax25packet->frametype, &ax25packet->ns, &ax25packet->nr, &ax25packet->pf, ax25packet->fExtseq);
	mheard.frametype[ frametype2ord(ax25packet->frametype) ]++;

	data   += ctlen;
	length -= ctlen;

	ax25packet->pdata = data;
	ax25packet->datalength = length;

	if ( ax25packet->frametype == I || ax25packet->frametype == UI ) {
		/* Decode I field */
		if ( length > 0 ) {	   /* Get pid */
			ax25packet->pid = *data++;
			length--;
			mheard.pid[ax25packet->pid]++;

/*  $TODO		if (ax25packet->pid == PID_SEGMENT) {
 *				 seg = *data++;
 *				 length--;
 *				 lprintf(T_AXHDR, "%s remain %u", seg & SEG_FIRST ? " First seg;" : "", seg & SEG_REM);
 *
 *				 if (seg & SEG_FIRST) {
 *					 ax25packet->pid = *data++;
 *					 length--;
 *				 }
 *			 }
 */
			mheard.nInfoBytes+=length;
			ax25packet->pdata = data;
			ax25packet->datalength = length;
			ax25packet->datacrc = calc_abincrc(data, length, 0x5aa5 );
		}
	} else if (ax25packet->frametype == FRMR && length >= 3) {
	/* FIX ME XXX
		lprintf(T_AXHDR, ": %s", decode_type(ftype(data[0])));
	*/
/*		  lprintf(T_AXHDR, ": %02X", data[0]);
 *  $TODO	 lprintf(T_AXHDR, " Vr = %d Vs = %d",
 *			 (data[1] >> 5) & MMASK,
 *			 (data[1] >> 1) & MMASK);
 *		 if(data[2] & W)
 *			 lprintf(T_ERROR, " Invalid control field");
 *		 if(data[2] & X)
 *			 lprintf(T_ERROR, " Illegal I-field");
 *		 if(data[2] & Y)
 *			 lprintf(T_ERROR, " Too-long I-field");
 *		 if(data[2] & Z)
 *			 lprintf(T_ERROR, " Invalid seq number");
 *		 lprintf(T_AXHDR,"%s\n", dama);
 */
	 } else if ((ax25packet->frametype == SABM || ax25packet->frametype == UA) && length >= 2) {
		/* FlexNet transmits the QSO "handle" for header
		 * compression in SABM and UA frame data fields
		 */
/*		  lprintf(T_AXHDR," [%d]%s\n", (data[0] << 8) | data[1], dama);
 */
	}
	/* ok, all fine */
	ax25packet->valid = 1;
	return ax25packet;
}

void
ax25packet_dump(struct t_ax25packet *pax25packet, int dumpstyle)
{
	char *dama;

	lprintf(T_TIMESTAMP, "(%s) ", time2str(&pax25packet->time) );

	/* if( pax25packet->pQSO )
	 *   lprintf(T_ADDR,  "(%d) ",  pax25packet->pQSO->qsoid);
	 */

	lprintf(T_PORT,      "%s: ", pax25packet->port);

	/* Nobody will need this:
	 *   if( pax25packet->fcrc )	   lprintf(T_PORT, "CRC-");
	 *   if( pax25packet->fflexcrc )   lprintf(T_PORT, "flex:");
	 *   if( pax25packet->fsmack )	   lprintf(T_PORT, "smack:");
	 */
	/* ------------------------------------- */

	if( !pax25packet->valid ) {
		/* Something wrong with the header */
		lprintf(T_ERROR, "AX25: bad header!\n");
		data_dump(pax25packet->pdata, pax25packet->datalength, HEX);
		return ;
	}
	if (pax25packet->fExtseq) {
		lprintf(T_PROTOCOL, "EAX25: ");
	} else {
		lprintf(T_PROTOCOL, "AX25: ");
	}

	if(pax25packet->fDama) {
		dama = DAMA_STRING;
	} else {
		dama = NDAMA_STRING;
	}

	lprintf(T_FMCALL, "%s", ax25call2str(&pax25packet->fmcall));
	lprintf(T_ZIERRAT, "->");
	lprintf(T_TOCALL, "%s", ax25call2str(&pax25packet->tocall));

	if( pax25packet->ndigi > 0 ) {
		int i;
		lprintf(T_AXHDR, " v");
		for(i=0;i<pax25packet->ndigi;i++) {
		    lprintf(T_VIACALL," %s%s", ax25call2str(&pax25packet->digi[i]),
			    pax25packet->ndigirepeated == i ? "*" : "");
		}
	}

	lprintf(T_AXHDR, " <%s", decode_type(pax25packet->frametype));
	switch (pax25packet->cmdrsp) {
		case LAPB_COMMAND:
			lprintf(T_AXHDR, " C");
			if( pax25packet->pf ) lprintf(T_AXHDR, "+");
			break;
		case LAPB_RESPONSE:
			lprintf(T_AXHDR, " R");
			if( pax25packet->pf ) lprintf(T_AXHDR, "-");
			break;
		default:
			break;
	}
	if (pax25packet->ns > -1 )
		lprintf(T_AXHDR, " S%d", pax25packet->ns);
	if (pax25packet->nr > -1 )
		lprintf(T_AXHDR, " R%d", pax25packet->nr);
	lprintf(T_AXHDR, ">");

	if (pax25packet->frametype == I || pax25packet->frametype == UI) {
		/* Decode I field */
		lprintf(T_PROTOCOL," %s%s\n",dama, pid2str(pax25packet->pid));
		switch (pax25packet->pid) {
			case PID_NO_L3: /* the most likly case */
				ai_dump(pax25packet->pdata, pax25packet->datalength);
				break;
			case PID_VJIP06:
			case PID_VJIP07:
			case PID_SEGMENT:
				data_dump(pax25packet->pdata, pax25packet->datalength, dumpstyle);
				break;
			case PID_IP:
				ip_dump(pax25packet->pdata, pax25packet->datalength, dumpstyle);
				break;
			case PID_ARP:
				arp_dump(pax25packet->pdata, pax25packet->datalength);
				break;
			case PID_NETROM:
				netrom_dump(pax25packet->pdata, pax25packet->datalength, dumpstyle);
				break;
			case PID_X25:
				rose_dump(pax25packet->pdata, pax25packet->datalength, dumpstyle);
				break;
			case PID_TEXNET:
				data_dump(pax25packet->pdata, pax25packet->datalength, dumpstyle);
				break;
			case PID_FLEXNET:
				flexnet_dump(pax25packet->pdata, pax25packet->datalength, dumpstyle);
				break;
			default:
				ai_dump(pax25packet->pdata, pax25packet->datalength);
				break;
		}
	} else if (pax25packet->frametype == FRMR && pax25packet->datalength >= 3) {
	/* FIX ME XXX
		lprintf(T_AXHDR, ": %s", decode_type(ftype(data[0])));
	*/
		lprintf(T_AXHDR, ": %02X", pax25packet->pdata);
		lprintf(T_AXHDR, " Vr = %d Vs = %d",
			(pax25packet->pdata[1] >> 5) & MMASK,
			(pax25packet->pdata[1] >> 1) & MMASK);
		if(pax25packet->pdata[2] & W)
			lprintf(T_ERROR, " Invalid control field");
		if(pax25packet->pdata[2] & X)
			lprintf(T_ERROR, " Illegal I-field");
		if(pax25packet->pdata[2] & Y)
			lprintf(T_ERROR, " Too-long I-field");
		if(pax25packet->pdata[2] & Z)
			lprintf(T_ERROR, " Invalid seq number");
		lprintf(T_AXHDR,"%s\n", dama);
	} else if ((pax25packet->frametype == SABM || pax25packet->frametype == UA) && pax25packet->datalength >= 2) {
		/* FlexNet transmits the QSO "handle" for header
		 * compression in SABM and UA frame data fields
		 */
		/* lprintf(T_AXHDR," [%d]%s\n", (pax25packet->pdata << 8) | pax25packet->pdata[1], dama); */
	} else {
		lprintf(T_AXHDR,"%s\n", dama);
	}
}



static char *
decode_type(int type)
{
	switch (type) {
		case I:     return "I";
		case SABM:  return "C";
		case SABME: return "CE";
		case DISC:  return "D";
		case DM:    return "DM";
		case UA:    return "UA";
		case RR:    return "RR";
		case RNR:   return "RNR";
		case REJ:   return "REJ";
		case FRMR:  return "FRMR";
		case UI:    return "UI";
		default:    return "[invalid]";
	}
}

int
mem2ax25call(struct t_ax25call *presbuf, unsigned char *data)
{
	int i;
	char *s;
	char c;

	s = presbuf->sCall;
	memset(s,0,sizeof(*presbuf));
	for (i = 0; i < ALEN; i++) {
		c = (data[i] >> 1) & 0x7F;
		if (c != ' ') *s++ = c;
	}
	/* *s = '\0'; */
	/* decode ssid */
	presbuf->ssid = (data[ALEN] & SSID) >> 1;
	return 0;
}



char *
pax25(char *buf, unsigned char *data)
/* shift-ASCII-Call to ASCII-Call */
{
	int i, ssid;
	char *s;
	char c;

	s = buf;

	for (i = 0; i < ALEN; i++) {
		c = (data[i] >> 1) & 0x7F;

		if (!isalnum(c) && c != ' ') {
			strcpy(buf, "[invalid]");
			return buf;
		}

		if (c != ' ') *s++ = c;
	}
	/* decode ssid */
	if ((ssid = (data[ALEN] & SSID)) != 0)
		sprintf(s, "-%d", ssid >> 1);
	else
		*s = '\0';

	return(buf);
}


static int
ftype(unsigned char *data, int *type, int *ns, int *nr, int *pf, int extseq)
/* returns then length of the controlfield, this could be > 1 ... */
{
	*nr = *ns = -1; /* defaults */
	if (extseq) {
		if ((*data & 0x01) == 0) {	/* An I frame is an I-frame ... */
			*type = I;
			*ns   = (*data >> 1) & 127;
			data++;
			*nr   = (*data >> 1) & 127;
			*pf   = *data & EPF;
			return 2;
		}
		if (*data & 0x02) {
			*type = *data & ~PF;
			*pf   = *data & PF;
			return 1;
		} else {
			*type = *data;
			data++;
			*nr   = (*data >> 1) & 127;
			*pf   = *data & EPF;
			return 2;
		}
	} else { /* non extended */
		if ((*data & 0x01) == 0) {	/* An I frame is an I-frame ... */
			*type = I;
			*ns   = (*data >> 1) & 7;
			*nr   = (*data >> 5) & 7;
			*pf   = *data & PF;
			return 1;
		}
		if (*data & 0x02) {		/* U-frames use all except P/F bit for type */
			*type = *data & ~PF;
			*pf   = *data & PF;
			return 1;
		} else {			/* S-frames use low order 4 bits for type */
			*type = *data & 0x0F;
			*nr   = (*data >> 5) & 7;
			*pf   = *data & PF;
			return 1;
		}
	}
}


void
ax25_dump(struct t_ax25packet *pax25packet, unsigned char *data, int length, int dumpstyle)
{
    ax25_decode( pax25packet, data, length );
    /* look for matching QSO */
    pax25packet->pQSO = doQSOMheard(pax25packet);
    tryspy(pax25packet);

    /* switch temp. all those clients off, which do not want to see supervisor
     * frames, if it is not an I-Frame. Yes. This is hacklike... */
    clientEnablement( ceONLYINFO, (pax25packet->pid > -1) );
    clientEnablement( ceONLYIP,   (pax25packet->pid == PID_IP) );
    ax25packet_dump( pax25packet, dumpstyle );
    /* and switch all disabled clients on */
    clientEnablement( ceONLYIP,   1 );
    clientEnablement( ceONLYINFO, 1 );
}

