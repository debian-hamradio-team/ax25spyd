/*
 *  ax25spyd - An AX.25 monitorspy daemon
 *
 *  Copyright (C) 1999 Free Software Foundation, Inc.
 *  Copyright (C) 1999 Walter Koch, dg9ep
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * $Id: spy.c,v 1.7 1999/07/13 22:26:12 walter Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <syslog.h>

#include "monixd.h"


static void doSpy( struct t_ax25packet* pax25, struct t_qso* pQSO);

/*------------- Client & Spy ------------------------------------------------*/

void
foundSpyQSO( int iClient, struct t_qso* pQSO)
/* open a new spy for iClient on pQSO */
{
    if( pQSO == NULL )
	return;
    if( iClient > MAXCLIENTPERQSO ) { /* too many clients */
	syslog(LOG_ERR, "cannot create spy, too many clients... %d",iClient);
	return;
    }
    if( pQSO->spyclient[iClient] )
	return; /* already enabled */
    pQSO->lastSpyedNumber = -1; /* reset, because we start spying now */
    pQSO->spyclient[iClient] = 1;
    if( fVerbose )
	syslog(LOG_INFO, "created spy on qso %d for client %d",pQSO->qsoid,iClient);
    /* give the client some feedback */
    lwrite( iClient, T_SPYHEADER, pQSO, sizeof(*pQSO) );
    /* transmit the firstheard-packet */
    if( pQSO->pFirstPacket ) {
	 doSpy( pQSO->pFirstPacket, pQSO);
    }
}


int
startSpyNewClientQsoId( int iClient, t_qsoid id )
/* Start spy on qsoid id for client iClient */
{
    struct t_qso* p;

    p = searchQSOFromQSOid( id );
    if( p == NULL )
	return 0; /* not found */
    foundSpyQSO(iClient, p);
    if( p->pOtherqso && client[iClient].fAutoSpyOtherID )
	foundSpyQSO(iClient, p->pOtherqso);
    return 1;
}

int
stopSpyClientQsoId( int iClient, t_qsoid id )
/* Stop spy on qsoid id for client iClient */
{
    struct t_qso* p;

    p = searchQSOFromQSOid( id );
    if( p == NULL ) { /* not found */
	if( fVerbose > 1 )
	   syslog(LOG_INFO, "can't close spy (qsoid %d): qso does not exist",id);
	return 0;
    }
    p->spyclient[iClient] = 0;
    lwrite( iClient, T_SPYEND, p, sizeof(*p) );
    if( fVerbose )
	syslog(LOG_INFO, "closed spy for qso %d for client %d",p->qsoid,iClient);
    return 1;
}


void
startSpyNewQSO( struct t_qso* pNewQSO )
/* Will be called, if new or till now unknown QSO is detected */
{
    int i;
    /* search all Clients weather they want to spy the new QSO pNewQSO
     * (filterexpression) */
    for(i=0;i<nClient;i++)
	 if( client[i].sSpyFilter )
	     if(    !memcmp(client[i].sSpyFilter, pNewQSO->fmcall.sCall, strlen(client[i].sSpyFilter) )
		 || !memcmp(client[i].sSpyFilter, pNewQSO->tocall.sCall, strlen(client[i].sSpyFilter) ) ) {
		     foundSpyQSO(i, pNewQSO);
	     }

    /* Now look for clients, which want to be informed about all new qsos */
    for(i=0;i<nClient;i++)
	if( client[i].fOfferNewQSO )
	    if( (pNewQSO->otherqsoid==0) || (client[i].fOfferAllNewQSO) ) {
	       /* offer only if the otherqso has not already be offerd (this
		* could have been disabled) */
		lwrite( i, T_QSOOFFER, pNewQSO, sizeof(*pNewQSO) );
		if( fVerbose )
		    syslog(LOG_INFO, "offer qso %d to client %d",pNewQSO->qsoid,i);
	    }

    /* If the otherqso is already spyed, then pNewQSO must bes spyed too */
    if( pNewQSO->pOtherqso )
	for(i=0;i<nClient;i++)
	    if( client[i].fAutoSpyOtherID ) {
		if( pNewQSO->pOtherqso->spyclient[i] ) {
		    if( fVerbose > 1 )
			syslog(LOG_INFO, "autospy because QSO %d is already spyed",pNewQSO->pOtherqso->qsoid);
		    foundSpyQSO(i, pNewQSO);
		}
    }
}


void
startSpyNewClient( int iClient, char *sFilter )
/* search all QSO wheather they want to be spied by new client */
{
    struct t_qso* p;
    int id;

    if( (id = atoi(sFilter)) > 0 ) { /* qsoid issued */
	if( startSpyNewClientQsoId(iClient, id) )
	    return; /* found */
    }

    for( p = pQSOMheardRoot; p != NULL; p = p->pNext )
	if(    !memcmp(sFilter, p->fmcall.sCall, strlen(sFilter) )
	    || !memcmp(sFilter, p->tocall.sCall, strlen(sFilter) ) ) {
		foundSpyQSO(iClient, p);
	}
}


/*----------------------------------------------------------------------*/

static void
mailSpy( struct t_ax25packet* pax25, struct t_qso* pQSO)
{
    /* check for Mailheader */
    /**/
    /* Look for Mailbeacon
     *	  if( (pax25->pid == PID_TEXT) && (pax25->frametype == UI) ) {
     *	      tryspyHeader(pax25,NULL,"MAIL");
     *	      tryspyHeader(pax25,NULL,"MAILS");
     *	  }
     */
}

static void
spyToFile( struct t_ax25packet* pax25, struct t_qso* pQSO)
/* write data of pax25 to file.
 * pax25 contains a packet in correct order
 * This is really a HACK */
{
    FILE *f;
    char filename[90];
    /* $TODO PERFORMANCE */
    sprintf(filename, AX25VARDIR"/ax25spyd/%s-%d__%s-%d",
		       pQSO->fmcall.sCall, pQSO->fmcall.ssid,
		       pQSO->tocall.sCall, pQSO->tocall.ssid
    );
    if( (f = fopen(filename, "a")) == NULL) {
	syslog(LOG_ERR, "can't open spyfile %s",filename);
	return;
    }
    /* $TODO WE CHANGE THE ORIGINAL PACKET DATA HERE ! AARGGHH! */
    convertBufferToReadable( pax25->pdata,  pax25->datalength );
    fwrite( pax25->pdata, pax25->datalength, 1, f);
    fclose(f);
}


static void
doSpy( struct t_ax25packet* pax25, struct t_qso* pQSO)
/* if this QSO is spyed, write data to the client.
 * *pax25 belongs to *pQSO.  packet is the "right" one.
 */
{
    if( pax25->datalength <= 0 ) {
	return;
    }

    tryspydxcluster(pax25);

    /* write to clients */
    lwriteSpy( pQSO, pax25->pdata, pax25->datalength );

    /* High level spying */
    if( fDoMailSpy )
	mailSpy( pax25, pQSO ); /* todo fill linebuffer */

    if( fSpyAllToFile )
	spyToFile( pax25, pQSO ); /* changes the packet! */

    /* erase first packet, if it still exists. This is JAK (Just another kludge) */
    if( pQSO->pFirstPacket ) {
	 /* erase only if newer packet is already spyt */
	 if( pQSO->frametype[ORD_I]+pQSO->frametype[ORD_UI] > 1 ) {
	      if( pQSO->pFirstPacket->pdata );
		  free(pQSO->pFirstPacket->pdata);
	      free(pQSO->pFirstPacket);
	      pQSO->pFirstPacket = NULL;
	 }
    }
}



static void
flushSpy( struct t_qso* pQSO, int von, int bis )
/* "writes" collected frames from *von* until *bis* (inlcusive)
 * -1,-1 -> flushall */
{
}

/*-------------------------------------------------------------------*/

void
tryspy( struct t_ax25packet* pax25 )
/* called by ax25_dump (ax25dump.c) */
{
    struct t_qso* pQSO;
    int ns;

    if( !pax25->valid )
	return;

    /* look for matching QSO */
    if( (pQSO = pax25->pQSO) == NULL )
	return; /* should'nt happen */

    /* end of QSO ? */
    if( pax25->frametype == DISC ) {
	flushSpy(pQSO,-1, -1);
	delQSO( pax25->pQSO );
	pax25->pQSO = NULL;
	return;
    }

    if( (pax25->frametype != I) && (pax25->frametype != UI) ) {
	/* we are only interested in QSOs which transfer data */
	return;
    }

    /* update mheard data */
    doCallMheard(pax25);

    if( pax25->frametype == UI ) { /* UIs do not have numbers... */
       /* so we can process data now. No datacrc check, because
	* the (pure Existenz) of a UI-Frame may be an important information.
	* e.g. Netrom-Nodes-Broadcast.
	*/
       doSpy(pax25,pQSO);
       return;
    }
    /* Only I-Frames with data beyond this point */
    ns = pax25->ns;
    if( pQSO->crcPacketCollect[ns] == pax25->datacrc )
	return; /* got that packet already */
    pQSO->crcPacketCollect[ns] = pax25->datacrc;

    if( ((pQSO->lastSpyedNumber+1) % 8) != ns ) {
	/* wrong order, store it for later use */
	/* $todo */
	return;
    }
    {
	/* Hier ist nun ein wirklich neues I-Paket eingetroffen.
	 * Dadurch muss spätestens jetzt das "alte" Pakete mit der gleichen Nr.
	 * wie das aktuelle verarbeitet werden */
	flushSpy(pQSO, ns, ns );

	/* Here is pax25 in the correct order */

	pQSO->lastSpyedNumber = ns;
	doSpy(pax25,pax25->pQSO);

/*	  if( pQSO->pPacketCollect[ns] ) {
 *	       if( pQSO->pPacketCollect[ns]->pdata );
 *		   free(pQSO->pPacketCollect[ns]->pdata);
 *	       free(pQSO->pPacketCollect[ns]);
 *	       pQSO->pPacketCollect[ns] = NULL;
 *	  }
 */

	ns = (ns + 1) % 8;
    }

} /* end tryspy */


