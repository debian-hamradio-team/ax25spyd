/*
 *  ax25spyd - An AX.25 monitorspy daemon
 *  Copyright (C) 1999 Free Software Foundation, Inc.
 *  Copyright (C) 1999 Walter Koch, dg9ep
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*
 * $Id: monixd.h,v 1.27 1999/07/13 22:26:11 walter Exp $
 */

#define CONFFILE AX25CONFDIR"/ax25spyd.conf"

#define MAXCLIENTS  50
/* size of ax-25 adressfield for one call */
#define AXLEN		7
/* size of ax25 callfield inside the adressfield */
#define ALEN		6


#include <unistd.h>
#ifdef __GLIBC__
#include <net/if.h>
#else
#include <linux/if.h>
#endif
#include <linux/if_ether.h>

#include "mondefs.h"
#include "monutil.h"



/* in monixd */
extern struct t_client client[MAXCLIENTS];
extern int nClient ;
extern int fVerbose ;
extern int fForceNoKiss ;

/* in mheard.c */
extern struct t_mheard mheard;
extern struct t_CallMheard *pCallMheardRoot;
extern struct t_qso  *pQSOMheardRoot;

extern int fDaemon;
extern int fSpyAllToFile;
extern int fDoMailSpy;


/*----------------------------------------------------------------------*/

struct t_client {
    int socket;   /* The connection to the client */
    /* struct sockaddr conn_addr; */
    /* int conn_addrlen; */
    int prot;	  /* Welches Protokol auf dem Socket ? */
#define cCLIENT_PROT_PREFIXED	1
#define cCLIENT_PROT_PLAIN_TEXT 2
#define cCLIENT_PROT_PLAIN_BUF	3
    int fShow;		/* Bool: show all strings; see ClientEnable() */
    int fRawPacket;	/* Bool: transfer _all_ recvd packets raw to the client */
    int fMoni;		/* Bool: Ist der Monitor f�r diesen Client aktiv? */
    int fTimestamp;	/* timestamp �bertragen? */
    int fOnlyHeaders;	/* show only Packetheaders */
    int fOnlyIFrames;	/* show only i-Frames */
    int fOnlyIP;	/* show only IP-Frames */
    int fdxclusterspy;	/* Monitors DX de ... */
    int fOfferNewQSO;	 /* announce new detected QSOs */
    int fOfferAllNewQSO; /* announce new detected QSOs, even if the otherqso has already been offerd */
    int fAutoSpyOtherID; /* Automagicaly spy the other qso too, if it is availble */
    char *sSpyFilter;
    char *sHeaderFilter;
};



/* In monixd.c */
void tcprint(int dtype, char *str, int size);
void startSpyNewQSO( struct t_qso* );
#define ASCII		0
#define HEX		1
#define READABLE	2
void data_dump(unsigned char *, int, int);
void hex_dump(unsigned char *data, int length);
void ascii_dump(unsigned char *data, int length);
void readable_dump(unsigned char *data, int length);
void ai_dump(unsigned char *data, int length) ;

void lwriteAll(int dtype, const void *buf, size_t size);
void lwrite(int iClient, int dtype, const void *buf, size_t size);
void lprintf(int dtype, char *fmt, ...);
void lprintfClient(int iClient, int dtype, char *fmt, ...);
void lwriteSpy(struct t_qso*, const void *, size_t);

#if STATICLINK
int initDaemon(void);
#endif

#define ceONLYINFO 1
#define ceONLYIP   2
void clientEnablement( int why, int enable );

/* In mheard.c */
void tryspy(struct t_ax25packet* );
void mheard_init(void);
void sendMHeard(int);
void sendCallMHeard(int, char*);
void sendQSOMHeard(int, char*);
void doCallMheard(struct t_ax25packet*);
void tryspydxcluster(struct t_ax25packet*);
struct t_qso* doQSOMheard(struct t_ax25packet* pax25);
void qsoclientIndexDeleted( int );
void qsoclientIndexChanged( int, int );
void delQSO( struct t_qso * );
struct t_qso* searchQSOFromQSOid( t_qsoid );

/* in spy.c */
void foundSpyQSO( int, struct t_qso*);
void startSpyNewQSO( struct t_qso*  );
int startSpyNewClientQsoId( int , int );
int stopSpyClientQsoId( int , int );
void startSpyNewClient( int , char* );

/* crc.c */
void init_crc(void);
int calc_abincrc(char* , int , unsigned int );

/* In kissdump.c */
void kiss_dump(struct t_ax25packet*, unsigned char *, int, int);
void dev_dump( struct ifreq *, unsigned char *data, int length);

/* ax25dump.c */
void ax25_dump( struct t_ax25packet*, unsigned char *, int, int);
char *pax25(char *, unsigned char *);

/* In nrdump.c */
void netrom_dump(unsigned char *, int, int);

/* In arpdump.c */
void arp_dump(unsigned char *, int);

/* In ipdump.c */
void ip_dump(unsigned char *, int, int);

/* In icmpdump.c */
void icmp_dump(unsigned char *, int, int);

/* In udpdump.c */
void udp_dump(unsigned char *, int, int);

/* In dnsdump.c */
void dns_dump(unsigned char *, int);

/* In tcpdump.c */
void tcp_dump(unsigned char *, int, int);

/* In rspfdump.c */
void rspf_dump(unsigned char *, int);

/* In ripdump.c */
void rip_dump(unsigned char *, int);

/* In rosedump.c */
void rose_dump(unsigned char *, int, int);

/* In flexnetdump.c */
void flexnet_dump(unsigned char *, int, int);

