/* UDP packet tracing
 * Copyright 1991 Phil Karn, KA9Q
 * used for ax25spyd. dg9ep, 1999
 * $Id: dnsdump.c,v 1.1 1999/05/15 20:20:47 walter Exp $
 */

#include <stdio.h>
#include "monixd.h"


/* Dump a DNS-Packet header */
void
dns_dump(unsigned char *data, int length)
{

	lprintf(T_TCPHDR, " DNS-Data:");
	lprintf(T_HDRVAL, "%d\n", length);
	data_dump(data, length, ASCII); /* $TODO */

}

