/*
 *  ax25spyd - an ax.25 monitor & spy daemon
 *
 *  Copyright (C) 1999 Free Software Foundation, Inc.
 *  Copyright (C) 1999 Walter Koch, dg9ep
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* monixd.c -  Mainpart of the ax25spyD - project
 * $Id: monixd.c,v 1.31 1999/07/13 22:26:11 walter Exp $
 */

#include <sys/types.h>
#include <sys/socket.h>
#ifdef __GLIBC__
#include <net/if.h>
#else
#include <linux/if.h>
#endif
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <netdb.h>
#include <syslog.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <signal.h>
#include <stdarg.h>
#include <errno.h>
#include <fcntl.h>

#include "monixd.h"
/* see makefile (AX25INCDIR): */
#include <config.h>

/* by request of Jochen :-) */
#include <sys/socket.h>
/*#include <linux/ax25.h>*/

#ifdef HAVE_NETAX25_AX25_H
#include <netax25/ax25.h>
#include <netrose/rose.h>
#include <netrom/netrom.h>
#else
#include <linux/ax25.h>
#include <linux/rose.h>
#include <linux/netrom.h>
#endif

#ifdef HAVE_NETAX25_AXLIB_H
   #include <netax25/axconfig.h>
   #include <netax25/axlib.h>
   #include <netax25/daemon.h>
#elif HAVE_AX25_AXUTILS_H
   #include <ax25/axconfig.h>
   #include <ax25/axutils.h>
   #include <ax25/daemon.h>
#else
   #include <axconfig.h>
   #include <axutils.h>
   #include <daemon.h>
#endif



#include "sockets.h"

/*----------------------------------------------------------------------*/
void handleSockPR(void);
void handleSockListen(int,int);
void handleClientCommands(int sockClient);
void doCmdLine( char*, int , int );

void lwriteHeader( int iClient, int dtype, size_t size);
void lwriteData( int iClient, const void *buf, size_t size);
int  lwriteCheck( int iClient );


#define BUFSIZE  3500


int fDaemon = 0;
int fVerbose = 0;
int fQuiet = 0;
int fShowAlien = 0;
int fAllowRemoteAlien = 0;
int fDoMailSpy = 0;
int fSpyAllToFile = 0;
int fForceNoKiss = 0;

char sVersion[80];

int nClient = 0;
struct t_client  client[MAXCLIENTS];

int highest_sock_fd;
int sockPR, sockListen, sockListenPlainText, sockListenPlainBuf;

char *port = NULL, *dev = NULL;
int proto = ETH_P_ALL ;
static int end = 0;

/* ---------------------------------------------------------------------- */

void
delSockClientIndex(int iClient, const char* sGrund)
{
    if( fVerbose )
	syslog(LOG_DEBUG, "connection %d, socket %d disconnected (%s)",
			   iClient, client[iClient].socket, sGrund );
    close(client[iClient].socket);
    client[iClient].socket = -1;
    free(client[iClient].sHeaderFilter);
    free(client[iClient].sSpyFilter);
    /* swap last entry with the one just deleted */
    client[iClient] = client[nClient];
    nClient--;
    /* CHANGE ALL QSOs */
    qsoclientIndexDeleted(iClient);
    qsoclientIndexChanged(nClient,iClient);
}

static void
handleSockPRData( char* buffer , int size , struct sockaddr *psa )
/* something happend on the monitor-socket */
{
    int fShowIt=1;
    struct ifreq ifr;
    int dumpstyle = READABLE;
    struct t_ax25packet ax25packet;

    /* get ax.25-Packet */
/*    if ((size = recvfrom(sockPR, buffer, sizeof(buffer), 0, &sa, &asize)) == -1) {
 *	  perror("ax25.recvfrom");
 *	  return;
 *    }
 */

    fShowIt = 1;
    /* Device filter */
    if (dev != NULL && strcmp(dev, psa->sa_data) != 0)
	fShowIt = 0;

    if (proto == ETH_P_ALL) {
	strcpy(ifr.ifr_name, psa->sa_data);
	if (ioctl(sockPR, SIOCGIFHWADDR, &ifr) < 0)
	    perror("ax25spyd.GIFADDR");

	if (ifr.ifr_hwaddr.sa_family != AF_AX25) {
	    if( fShowAlien ) /* foreign stuff - bahh! */
		dev_dump( &ifr, buffer, size );
	    fShowIt = 0;
	}
    }

    if( fShowIt ) {
	int i;

	for(i=0;i<nClient;i++) {
	   if( client[i].prot == cCLIENT_PROT_PLAIN_BUF ) {
		/* writing to plaintext-Socket */
		int n;
		n = write(client[i].socket,buffer,size);
		if( n==-1 ) {
		    char buffer[200];
		    sprintf(buffer,"pr.writeplain: size: %d, errNo:%d",size,errno);
		    delSockClientIndex(i,"broken Pipe (pr.writeplain)");
		    return;
		}
	   }
	   if( client[i].fRawPacket ) {
	       if( !lwriteCheck(i) ) {
		   lwriteHeader( i, T_RAWPACKET, size+sizeof(*psa)  );
		   lwriteData( i, psa, sizeof(*psa) );
		   lwriteData( i, buffer, size );
	       }
	   }
	}

	memset( &ax25packet, 0, sizeof(ax25packet) );
	time(&ax25packet.time);

	if ((ax25packet.port = ax25_config_get_name(psa->sa_data)) == NULL)
	    ax25packet.port = psa->sa_data;
	if( size > 0 ) {
	    kiss_dump( &ax25packet, buffer, size, dumpstyle );
	}
    }
} /**/


void
handleSockPR(void)
/* something happend on the monitor-socket */
{
    unsigned char buffer[BUFSIZE];
    struct sockaddr sa;
    int asize = sizeof(sa);
    int size;

    /* get ax.25-Packet */
    if ((size = recvfrom(sockPR, buffer, sizeof(buffer), 0, &sa, &asize)) == -1) {
	perror("ax25.recvfrom");
	return;
    }
    handleSockPRData( buffer , size, &sa);
}


void
handleSockListen(int prot, int sock)
{
    int sockClient;
    struct sockaddr conn_addr;
    int conn_addrlen;

    if( nClient >= MAXCLIENTS ) {
	syslog(LOG_WARNING,
	       "too many clientconnects - only %d possible\n", MAXCLIENTS);
	return;
    }

    sockClient = accept( sock, &conn_addr, &conn_addrlen);
    if(sockClient == -1) {
	syslog(LOG_WARNING, "accept() on server socket failed");
	return;
    }
    if( sockClient > highest_sock_fd )
	highest_sock_fd = sockClient;
    /* fcntl(sockClient,F_SETFL,O_NONBLOCK); */

    if( fVerbose )
	syslog(LOG_DEBUG, "new connection %d from client to socket %d",nClient,sockClient);

    client[nClient].socket = sockClient;
    client[nClient].prot   = prot;
    /* Plainsockets means "no MONISTART-Cmd needed:" */
    client[nClient].fMoni  = (prot != cCLIENT_PROT_PREFIXED);
    client[nClient].fShow  = 1;
    client[nClient].fTimestamp = 0;
    client[nClient].fOnlyHeaders = 0;
    client[nClient].sHeaderFilter = NULL;
    client[nClient].sSpyFilter = NULL;
    nClient++;
}


void
handleClientCommands(int iClient)
{
    int size;
    unsigned char buffer[200];
    struct t_protHeader protHeader;

    int sockClient = client[iClient].socket;

    if( sockClient < 0 ) { /* it is invalid */
	delSockClientIndex(iClient,"Invalid");
	return;
    }

    /* Lese Type und Gr�sse */
    size = readfull(sockClient,&protHeader,sizeof(protHeader));
    if( size < 1 ) {
	sprintf(buffer,"handleClientCommands.readprotheader: size: %d, errNo.%d",size,errno);
	delSockClientIndex(iClient,buffer);
	return;
    }

    if( protHeader.dsize > 0 ) {
	/* und nun die eigentlichen Daten lesen */
	if( (size = readfull(sockClient, buffer, protHeader.dsize)) == -1) {
	    sprintf(buffer,"handleClientCommands.readdata: size: %d, errNo.%d",size,errno);
	    delSockClientIndex(iClient,buffer);
	    return;
	}
	if( protHeader.dtype == TT_CMD_FOLLOWS ) {
	    doCmdLine(buffer,size,iClient);
	}
    }
}


/*----------------------------------------------------------------------*/
void
lwriteHeader( int iClient, int dtype, size_t size)
{
    struct t_protHeader protHeader;
    int n;

    if( client[iClient].prot == cCLIENT_PROT_PREFIXED ) {
	/* Protokol Header schreiben */
	protHeader.version = 1;
	protHeader.dtype = dtype;
	protHeader.dsize = size;
	n = write( client[iClient].socket,&protHeader,sizeof(protHeader) );
	if( n==0 ) {
	    syslog(LOG_DEBUG, "lwrite.writeprotheader: dtype=%d size=%d",dtype,size);
	}
	if( n==-1 ) {
	    char buffer[200];
	    sprintf(buffer, "write.writeprotheader: size: %d, errNo.%d",size,errno);
	    delSockClientIndex(iClient,buffer);
	    return;
	}
    }
}

void
lwriteData( int iClient, const void *buf, size_t size)
{
    int n;
    if( size > 0 ) {
	n = write( client[iClient].socket, buf,size );
	if( n==0 ) {
	    syslog(LOG_DEBUG, "lwrite.writeprotdata: size=%d",size);
	}
	if( n==-1 ) {
	    char buffer[200];
	    sprintf(buffer,"lwrite.writeprotdata: size: %d, errNo.%d",size,errno);
	    delSockClientIndex(iClient,buffer);
	}
    }
}

int
lwriteCheck( int iClient )
{
    if( client[iClient].socket < 0 )
	return 1;
    if( !client[iClient].fShow )
	return 1;
    if( client[iClient].prot == cCLIENT_PROT_PLAIN_BUF )
	return 1; /* they got their data already */
    return 0;
}


void
lwrite( int iClient, int dtype, const void *buf, size_t size)
{
    if( lwriteCheck(iClient) )
	return;
    lwriteHeader( iClient, dtype, size );
    lwriteData( iClient, buf, size );
}


void
lprintfClient(int iClient, int dtype, char *fmt, ...)
{
    va_list args;
    char str[1024];

    va_start(args, fmt);
    vsnprintf(str, 1024, fmt, args);
    va_end(args);

    lwrite(iClient, dtype, str, strlen(str) );
}

void
lwriteAll(int dtype, const void *buf, size_t size)
{
    int i;
    for(i=0;i<nClient;i++)
	if( client[i].fdxclusterspy && (dtype==T_DXCLUST) ) {
	    lwrite(i, T_DXCLUST, buf, size);
	}
}


void
lwriteSpy(struct t_qso* pQSO, const void *buf, size_t size)
/* Write buf/size to all clients, which are spying pQSO */
{
    int i;

    if( pQSO->qsoid < 1 )
	return;
    for( i=0 ; i<nClient ; i++ )
	if( pQSO->spyclient[i] ) {
	    lwriteHeader( i, T_SPYDATA, sizeof(t_qsoid)+size );
	    lwriteData( i, &pQSO->qsoid, sizeof(t_qsoid) );
	    lwriteData( i, buf, size );
	}
}


void
lprintf(int dtype, char *fmt, ...)
{
    va_list args;
    char str[1024];
    int i;

    va_start(args, fmt);
    vsnprintf(str, 1024, fmt, args);
    va_end(args);

    for(i=0;i<nClient;i++)
	if( (client[i].fMoni)  /* Monitor an? */
	 && (( client[i].fTimestamp  ) || (dtype!=T_TIMESTAMP) )
	 && ((!client[i].fOnlyHeaders) || (dtype!=T_DATA     ) )
	  ) {
	    if( (client[i].sHeaderFilter != NULL) )
		if( strstr(str, client[i].sHeaderFilter)==NULL )
		    continue; /* filterausdruck nicht gefunden */

	    lwrite(i, dtype, str, strlen(str) );
	}
}
/* ---------------------------------------------------------------------- */

void
doCmdLine( char *buffer, int size, int iClient )
/* execute command in *buffer* given by client iClient
 * $TODO: write a parser */
{
    buffer[size] = '\0';

    if( fVerbose )
	syslog(LOG_DEBUG, "command '%s' from client %i",
			   buffer, iClient
	);

    if( strcmp(buffer,"RAWDATA") == 0 ) {
	/* RAWDATA <size> <sa-data> <pr-data> */
	handleSockPRData( buffer+sizeof(struct sockaddr),
			  size-sizeof(struct sockaddr)	,
			  (struct sockaddr*)buffer	       );
	return;
    }

    if( strcmp(buffer,"MHEARD") == 0 ) {
	sendMHeard(iClient);
	return;
    }

    if( strcmp(buffer,"QSOMHEARD") == 0 ) {
	sendQSOMHeard(iClient,NULL);
	return;
    }
    if( strncmp(buffer,"QSOMHEARD ",10) == 0 ) {
	sendQSOMHeard(iClient,buffer+10);
	return;
    }

    if( strcmp(buffer,"CALLMHEARD") == 0 ) {
	sendCallMHeard(iClient,NULL);
	return;
    }
    if( strncmp(buffer,"CALLMHEARD ",11) == 0 ) {
	sendCallMHeard(iClient,buffer+11);
	return;
    }

    if( strncmp(buffer,"SPYINCL ",8) == 0 ) {
	int siz = strlen(buffer)-8+1;
	if( (client[iClient].sSpyFilter = malloc(siz)) != NULL )
	    memcpy(client[iClient].sSpyFilter, buffer+8, siz);
	startSpyNewClient( iClient,client[iClient].sSpyFilter );
	return;
    }

    if( strncmp(buffer,"SPYID ",6) == 0 ) {
	int id,rc;
	id = atol(buffer+6);
	rc = startSpyNewClientQsoId(iClient,id);
	return;
    }
    if( strncmp(buffer,"SPYSTOPID ",10) == 0 ) {
	int id = atol(buffer+10);
	stopSpyClientQsoId(iClient,id);
	return;
    }
    if( strcmp(buffer,"AUTOSPYOTHER") == 0 ) {
	client[iClient].fAutoSpyOtherID = 1;
	return;
    }

    if( strncmp(buffer,"QUERYQSOID ",11) == 0 ) {
	int id = atol(buffer+11);
	struct t_qso* pQSO = searchQSOFromQSOid(id);
	if( pQSO )
	    lwrite( iClient, T_SPYHEADER, pQSO, sizeof(*pQSO) );
	return;
    }

    if( strcmp(buffer,"OFFERNEWQSO") == 0 ) {
	client[iClient].fOfferNewQSO	= 1;
	client[iClient].fOfferAllNewQSO = 0;
	client[iClient].fAutoSpyOtherID = 1;
	return;
    }
    if( strcmp(buffer,"OFFERALLNEWQSO") == 0 ) {
	/* Auch die QSOs der Gegenrichtung anbieten */
	client[iClient].fOfferNewQSO	= 1;
	client[iClient].fOfferAllNewQSO = 1;
	client[iClient].fAutoSpyOtherID = 0;
	return;
    }
    if( strcmp(buffer,"OFFERALLQSO") == 0 ) {
	/* Offer all QSOs we already have got */
	    struct t_qso* p;
	    /* r�ckw�rts w�r sch�ner :-) */
	    for( p = pQSOMheardRoot; p != NULL; p = p->pNext ) {
		lwrite( iClient, T_QSOOFFER, p, sizeof(*p) );
		if( fVerbose )
		    syslog(LOG_INFO, "offer qso %d to client %d",p->qsoid,iClient);
	    }
	return;
    }

    if( strcmp(buffer,"RAWPACKET") == 0 ) {
	client[iClient].fRawPacket = 1;
	return;
    }
    if( strcmp(buffer,"STARTMONI") == 0 ) {
	client[iClient].fMoni = 1;
	return;
    }
    if( strcmp(buffer,"STOPMONI") == 0 ) {
	client[iClient].fMoni = 0;
	return;
    }
    if( strcmp(buffer,"TIMESTAMP NO") == 0 ) {
	client[iClient].fTimestamp = 0;
	return;
    }
    if( strcmp(buffer,"TIMESTAMP YES") == 0 ) {
	client[iClient].fTimestamp = 1;
	return;
    }
    if( strcmp(buffer,"ONLYHEADERS") == 0 ) {
	client[iClient].fOnlyHeaders = 1;
	return;
    }
    if( strcmp(buffer,"ONLYHEADERS NO") == 0 ) {
	client[iClient].fOnlyHeaders = 0;
	return;
    }
    if( strcmp(buffer,"ONLYIFRAMES") == 0 ) {
	client[iClient].fOnlyIFrames = 1;
	return;
    }
    if( strcmp(buffer,"ONLYIFRAMES NO") == 0 ) {
	client[iClient].fOnlyIFrames = 0;
	return;
    }
    if( strcmp(buffer,"ONLYIP") == 0 ) {
	client[iClient].fOnlyIP = 1;
	return;
    }
    if( strcmp(buffer,"DXCLUSTERSPY") == 0 ) {
	client[iClient].fdxclusterspy = 1;
	return;
    }
    if( strncmp(buffer,"HEADERFILTER ",12) == 0 ) {
	int siz = strlen(buffer)-12+1;
	if( (client[iClient].sHeaderFilter = malloc(siz)) != NULL )
	    memcpy(client[iClient].sHeaderFilter, buffer+13, siz);
	if( fVerbose )
	     syslog(LOG_DEBUG, "client %d Filter: %s",iClient, client[iClient].sHeaderFilter);
	return;
    }

    if( strcmp(buffer,"SHOWALIEN") == 0 ) {
	fShowAlien = fAllowRemoteAlien;
	return;
    }
    if( strcmp(buffer,"SHOWALIEN NO") == 0 ) {
	fShowAlien = 0;
	return;
    }

    if( strcmp(buffer,"VERSION") == 0 ) {
	lwrite( iClient, T_VERSION, sVersion , strlen(sVersion) );
	return;
    }
    if( strcmp(buffer,"PROTVERSION") == 0 ) {
	int vers = PROTVERSION;
	lwrite( iClient, T_PROTVERSION, &vers , sizeof(vers) );
	return;
    }

    syslog(LOG_DEBUG, "unknown internal command %s from client %i",
		       buffer, iClient
    );
}

/*----------------------------------------------------------------------*/


void
clientEnablement( int why, int val )
{
    int i;
    for(i=0;i<nClient;i++) {
	if( why == ceONLYINFO )
	    if( client[i].fOnlyIFrames )
		client[i].fShow = val;
	if( why == ceONLYIP )
	    if( client[i].fOnlyIP )
		client[i].fShow = val;
    }
}

/*----------------------------------------------------------------------*/

/*-------dumpstuff----------*/

void
ascii_dump(unsigned char *data, int length)
/* Example:  "0000 Dies ist ein Test.."
 */
{
	unsigned char c;
	int  i, j;
	char buf[100];

	for (i = 0; length > 0; i += 64) {
		sprintf(buf, "%04X  ", i);

		for (j = 0; j < 64 && length > 0; j++) {
			c = *data++;
			length--;

			if ((c != '\0') && (c != '\n'))
				strncat(buf, &c, 1);
			else
				strcat(buf, ".");
		}

		lprintf(T_DATA, "%s\n", buf);
	}
}


void
readable_dump(unsigned char *data, int length)
/* Example "Dies ist ein Test.
 *	   "
 */
{
	unsigned char c;
	int  i;
	int  cr = 1;
	char buf[BUFSIZE];

	for (i = 0; length > 0; i++) {
		c = *data++;
		length--;
		switch (c) {
			case 0x00:
				buf[i] = ' ';
			case 0x0A: /* hum... */
			case 0x0D:
				if (cr) buf[i] = '\n';
				   else i--;
				break;
			default:
				buf[i] = c;
		}
		cr = (buf[i] != '\n');
	}
	if (cr)
		buf[i++] = '\n';
	buf[i++] = '\0';
	lprintf(T_DATA, "%s", buf);
}

void
hex_dump(unsigned char *data, int length)
/*  Example: "0000 44 69 65 ......               Dies ist ein Test."
 */
{
	int  i, j, length2;
	unsigned char c;
	char *data2;

	char buf[4], hexd[49], ascd[17];

	length2 = length;
	data2	= data;

	for (i = 0; length > 0; i += 16) {
		hexd[0] = '\0';
		for (j = 0; j < 16; j++) {
			c = *data2++;
			length2--;

			if (length2 >= 0)
				sprintf(buf, "%2.2X ", c);
			else
				strcpy(buf, "   ");
			strcat(hexd, buf);
		}

		ascd[0] = '\0';
		for (j = 0; j < 16 && length > 0; j++) {
			c = *data++;
			length--;

			sprintf(buf, "%c", ((c != '\0') && (c != '\n')) ? c : '.');
			strcat(ascd, buf);
		}

		lprintf(T_DATA, "%04X  %s | %s\n", i, hexd, ascd);
	}
}

void
data_dump(unsigned char *data, int length, int dumpstyle)
{
	switch (dumpstyle) {

	case READABLE:
		readable_dump(data, length);
		break;
	case HEX:
		hex_dump(data, length);
		break;
	default:
		ascii_dump(data, length);
	}
}

void
ai_dump(unsigned char *data, int length)
{
	int testsize, i;
	char c;
	char *p;
	int dumpstyle = READABLE;

	/* make a smart guess how to dump data */
	testsize = (10>length) ? length:10;
	p = data;
	for (i = testsize; i>0; i--) {
		c = *p++;
		if( iscntrl(c) && (!isspace(c)) ) {
		    dumpstyle = ASCII; /* Hey! real smart! $TODO */
		    break;
		}
	}
	/* anything else */
	data_dump(data, length, dumpstyle);
}


/*----------------------------------------------------------------------*/

void
readConfig(void)
/* not in use yet 28.06.99 */
{
    FILE *conffile;
    char confline[100];
    char *sockname;
    char *mode;

    /* read the configuration file */
    if(!(conffile=fopen(CONFFILE, "r"))) {
	syslog(LOG_INFO, "Unable to open conffile " CONFFILE ", using defaults");
	return;
    }

    while(fgets(confline, sizeof(confline), conffile)) {
	if(confline[0]=='#') /* ignore comment */
	    continue;

	confline[strlen(confline)-1]=0; /* Cut the EOL (LF) */

	if(!(sockname=strchr(confline, ' '))) {
	    fprintf(stderr, "WARNING: The following configuration line includes "
			    "one or more errors:\n%s\n", confline);
	    continue;
	}

	*(sockname++)=0;
	mode=confline;

/*#define MONIX_PORT  14090
 *#define MONIXPLAINTEXT_PORT  14091
 *#define MONIXPLAINBUF_PORT   14092
 */
	if(strcasecmp(mode, "CMDPORT")==0)
	    /* add_socket(sockname, 0) */;
	else if(strcasecmp(mode, "PLAINTEXTPORT")==0)
	    /* add_socket(sockname, 1) */ ;
	else if(strcasecmp(mode, "PLAINBUFPORT")==0)
	    ;
	else
	    fprintf(stderr, "WARNING: Mode \"%s\" not supported\n", mode);

    }
    fclose(conffile);
}

/*----------------------------------------------------------------------*/

void
daemon_quit_handler(int dummy)
{ /* called by ??? */
    syslog(LOG_INFO, "terminating on SIG %d",dummy);
    end=1;
}


int
#if STATICLINK
initDaemon(void)
#else
main(int argc, char **argv)
#endif
{
#if STATICLINK
#else
    int s;
#endif
    int i;

    end = 0;
    fDaemon = 1;

    init_crc();

    sprintf(sVersion,"ax25spyd %s - compiled: %s %s", VERSION, __DATE__,__TIME__);
    printf("\n%s\n", sVersion);

#if STATICLINK
#else

    while ((s = getopt(argc, argv, "adfpqsvV?")) != -1) {
	 switch (s) {
	    case 'a':
		    fShowAlien++;
		    break;
	    case 'd': /* leave it in the fg */
		    fDaemon = 0;
		    break;
	    case 'e':
		    fAllowRemoteAlien++;
		    break;
	    case 'f':
		    fForceNoKiss++;
		    break;
	    case 'm':
		    fDoMailSpy++;
		    break;
	    case 'p':
		    port = optarg;
		    break;
	    case 'q':
		    fQuiet++;
		    fVerbose = 0;
		    break;
	    case 's':
		    fSpyAllToFile++;
		    break;
	    case 'v':
		    fVerbose++;
		    break;
	    case 'V':
		    printf("\n%s\n\n", sVersion);
		    printf("This program is free software; you can redistribute it and/or modify\n"
			   "it under the terms of the GNU General Public License as published by\n"
			   "the Free Software Foundation; either version 2 of the License, or\n"
			   "(at your option) any later version.\n\n"
			   "This program is distributed in the hope that it will be useful,\n"
			   "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
			   "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\n");
		    return 0;
	    case '?':
		    fprintf(stderr, "Usage: ax25spyd [-p port] [-v] [-d (start as daemon)] \n");
		    return 1;
	 }
    }
#endif
    openlog("ax25spyd", LOG_PID, LOG_DAEMON);
    syslog(LOG_INFO, "started %s", sVersion);
    /* readConfig(); */

    if (ax25_config_load_ports() == 0)
	    fprintf(stderr, "ax25spyd: no ax.25-port configured\n");

    if (port != NULL) {
	if ((dev = ax25_config_get_dev(port)) == NULL) {
	    fprintf(stderr, "ax25spyd: invalid ax.25-port name - %s\n", port);
	    return 1;
	}
    }

    signal(SIGPIPE, SIG_IGN); /* damit wir bei write() etc. ein EPIPE statt SIG_ kriegen */
    setservent(1); /* open and rewind /etc/service */

    /* create ax.25-monitor socket */
    if( (sockPR = socket(AF_INET, SOCK_PACKET, htons(proto))) == -1 ) {
	perror("ax.25-socket");
	return 1;
    }
    highest_sock_fd = sockPR;

    /* create listen-for-clients-socket */
    sockListen = OpenServerSocket(MONIX_PORT);
    if(sockListen == -1) {
	syslog(LOG_ERR, "cannot create listen socket for port %d",MONIX_PORT);
	exit(1);
    }
    if( sockListen > highest_sock_fd )
	highest_sock_fd = sockListen;

    /* create listen-for-plain-text-clients socket */
    sockListenPlainText = OpenServerSocket(MONIXPLAINTEXT_PORT);
    if(sockListenPlainText == -1) {
	syslog(LOG_ERR, "cannot create listen socket for plaintext-port %d.",MONIXPLAINTEXT_PORT);
	exit(1);
    }
    if( sockListenPlainText > highest_sock_fd )
	highest_sock_fd = sockListenPlainText;

    /* create listen-for-plain-buf-clients-socket */
    sockListenPlainBuf = OpenServerSocket(MONIXPLAINBUF_PORT);
    if(sockListenPlainBuf == -1) {
	syslog(LOG_ERR, "cannot create listen socket for plainBuf-port %d.",MONIXPLAINBUF_PORT);
	exit(1);
    }
    if( sockListenPlainBuf > highest_sock_fd )
	highest_sock_fd = sockListenPlainBuf;

    if( fDaemon ) {
	if( !fQuiet ) printf(" running as daemon\n");
	if (!daemon_start(FALSE)) {
	    fprintf(stderr, "ax25spyd: can not become a daemon\n");
	    return 1;
	}
    } else {
	if( !fQuiet ) printf(" running in fg\n");
    }

    signal(SIGINT,  daemon_quit_handler);
    signal(SIGTERM, daemon_quit_handler);

    mheard_init();


    for (;!end;) {
	fd_set fdReadset;
	struct timeval tv;
	int res;

	/* Prepare select() */
	FD_ZERO(&fdReadset);
	FD_SET(sockPR,		   &fdReadset); /* is there PR-Data? */
	FD_SET(sockListen,	   &fdReadset); /* are there clients-connect-Requests? */
	FD_SET(sockListenPlainText,&fdReadset);
	FD_SET(sockListenPlainBuf, &fdReadset);
	for(i=0;i<nClient;i++) {
	    FD_SET(client[i].socket, &fdReadset); /* Are there commands from connected clients? */
	}

	tv.tv_sec=1; /* Wait 1 second (?), so ctrl-c takes effect in non-daemon mode */
	tv.tv_usec=0;
	res = select( highest_sock_fd+1, &fdReadset, 0, 0, &tv );
	if( res == -1 )
	    return 1;
	if( res > 0 ) {
	    /* check for ax25 data */
	    if( FD_ISSET(sockPR, &fdReadset) )
		handleSockPR();
	    /* check for new socket connect-requests: */
	    if( FD_ISSET(sockListen,	      &fdReadset) )
		handleSockListen(cCLIENT_PROT_PREFIXED,  sockListen);
	    if( FD_ISSET(sockListenPlainBuf,  &fdReadset) )
		handleSockListen(cCLIENT_PROT_PLAIN_BUF, sockListenPlainBuf);
	    if( FD_ISSET(sockListenPlainText, &fdReadset) )
		handleSockListen(cCLIENT_PROT_PLAIN_TEXT,sockListenPlainText);
	    /* check for cmds from already connected clients: */
	    for(i=0;i<nClient;i++)
		if( FD_ISSET(client[i].socket, &fdReadset) ) {
		    handleClientCommands(i);
		}
	}
    }

    syslog(LOG_INFO, "shutting down");
    close( sockPR );
    close( sockListen );
    close( sockListenPlainBuf );
    close( sockListenPlainText );
    for(i=0;i<nClient;i++) {
	delSockClientIndex( i, "ax25spyd shutdown");
    }
    return(0);
}


