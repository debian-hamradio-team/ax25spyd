/* TCP header tracing routines
 * Copyright 1991 Phil Karn, KA9Q
 * used for ax25spyd. dg9ep, 1999
 * $Id: tcpdump.c,v 1.5 1999/03/08 12:01:44 walter Exp $
 */

#include <stdio.h>
#include <string.h>

#include "monixd.h"

#define FIN     0x01
#define SYN     0x02
#define RST     0x04
#define PSH     0x08
#define ACK     0x10
#define URG     0x20
#define CE      0x40

/* TCP options */
#define EOL_KIND        0
#define NOOP_KIND       1
#define MSS_KIND        2
#define MSS_LENGTH      4

/* TCP SERVICES */
#define PORT_FTP_DATA 20
#define PORT_FTP      21
#define PORT_SMTP     25
#define PORT_DNS      53
#define PORT_WWW      80
#define PORT_POP3    110
#define PORT_NNTP    119

#define TCPLEN  20

#define max(a,b)  ((a) > (b) ? (a) : (b))


void
tcp_dump(unsigned char *data, int length, int hexdump)
/* Dump a TCP segment header. Assumed to be in network byte order */
{
        int source, dest;
        int seq;
        int ack;
        int flags;
        int wnd;
        int up;
        int hdrlen;
        int mss = 0;

        mheard.protTCP++;

        source = get16(data + 0);
        dest   = get16(data + 2);
        seq    = get32(data + 4);
        ack    = get32(data + 8);
        hdrlen = (data[12] & 0xF0) >> 2;
        flags  = data[13];
        wnd    = get16(data + 14);
        up     = get16(data + 18);

        lprintf(T_PROTOCOL, "TCP: ");

        /* the tcp-port is part of the adress (1.2.3.4:80), so we
         * use T_ADDR here instead of T_PROTOCOL. And it looks better, too */
        lprintf(T_ADDR, "%s", servname(source, "tcp"));
        lprintf(T_ZIERRAT, "->");
        lprintf(T_ADDR, "%s", servname(dest,   "tcp"));

        if (flags & CE)  lprintf(T_HDRVAL, " CE");
        if (flags & URG) lprintf(T_HDRVAL, " URG");
     /* if (flags & ACK) lprintf(T_HDRVAL, " ACK"); */
        if (flags & PSH) lprintf(T_HDRVAL, " PSH");
        if (flags & RST) lprintf(T_HDRVAL, " RST");
        if (flags & SYN) lprintf(T_HDRVAL, " SYN");
        if (flags & FIN) lprintf(T_HDRVAL, " FIN");

        lprintf(T_TCPHDR, " Seq:");
        lprintf(T_HDRVAL, "%x",  seq);
        if (flags & ACK) {
            lprintf(T_TCPHDR, " Ack:");
            lprintf(T_HDRVAL, "%x", ack);
        }

        lprintf(T_TCPHDR, " Wnd:", wnd);
        lprintf(T_HDRVAL, "%d", wnd);

        if (flags & URG) {
            lprintf(T_TCPHDR, " UP:");
            lprintf(T_HDRVAL, "x%x", up);
        }
        /* Process options, if any */
        if (hdrlen > TCPLEN && length >= hdrlen) {
                unsigned char *cp = data + TCPLEN;
                int i = hdrlen - TCPLEN;
                int kind, optlen;

                while (i > 0) {
                        kind = *cp++;

                        /* Process single-byte options */
                        switch (kind) {
                        case EOL_KIND:
                                i--;
                                cp++;
                                break;
                        case NOOP_KIND:
                                i--;
                                cp++;
                                continue;
                        }

                        /* All other options have a length field */
                        optlen = *cp++;

                        /* Process valid multi-byte options */
                        switch (kind) {
                        case MSS_KIND:
                                if (optlen == MSS_LENGTH)
                                        mss = get16(cp);
                                break;
                        }

                        optlen = max(2, optlen); /* Enforce legal minimum */
                        i -= optlen;
                        cp += optlen - 2;
                }
        }

        if (mss != 0) lprintf(T_TCPHDR," MSS %d", mss);

        length -= hdrlen;
        data   += hdrlen;

        if (length > 0) {
                lprintf(T_TCPHDR, " Data:");
                lprintf(T_HDRVAL, "%d\n", length);
                /* data_dump(data, length, hexdump); */
                switch(source) {
                  case PORT_NNTP:
                  case PORT_WWW:
                  case PORT_FTP:
                  case PORT_SMTP:
                  case PORT_POP3:
                           readable_dump(data, length);
                           break;
                  case PORT_FTP_DATA:
                           hex_dump(data, length);
                           break;
                  default: ascii_dump(data, length);
                }
                return;
        }

        lprintf(T_TCPHDR, "\n");
}

