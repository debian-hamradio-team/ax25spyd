/* Tracing routines for KISS TNC
 * Copyright 1991 Phil Karn, KA9Q
 * $Id: kissdump.c,v 1.9 1999/06/28 22:17:35 walter Exp $
 */

#include <stdio.h>
#ifdef __GLIBC__
#include <net/if.h>
#else
#include <linux/if.h>
#endif
#include <linux/if_ether.h>
#include "monixd.h"

#define PARAM_DATA	 0
#define PARAM_TXDELAY	 1
#define PARAM_PERSIST	 2
#define PARAM_SLOTTIME	 3
#define PARAM_TXTAIL	 4
#define PARAM_FULLDUP	 5
#define PARAM_HW	 6
#define PARAM_FIRSTUNDEF 7
#define PARAM_LASTUNDEF 14
#define PARAM_RETURN	15	 /* Should be 255, but is ANDed with 0x0F */



#define ISAX25ADDR(x) (((x >= 'A') && (x <= 'Z')) || ((x >= '0') && (x <= '9')) || (x == ' '))


void
kiss_dump( struct t_ax25packet *ax25packet, unsigned char *data, int length, int hexdump)
{
	int type;
	int val;
	int dev;

	type = data[0] & 0x0f;
	dev  = data[0] & 0xf0;

	/* till 2.0; 2.2 do not start with KISS ... */
	/* if(	data[0] >= 0x60  0x30  shifted	) { */
	if( fForceNoKiss || (ISAX25ADDR(data[0] >> 1)) ) {
	     /* this is not a kiss-packet, try ax25 */
	     ax25_dump(ax25packet, data, length, hexdump);
	     return;
	}

	ax25packet->fsmack   = data[0] & 0x80;
	ax25packet->fflexcrc = data[0] & 0x20;
	ax25packet->fcrc = ax25packet->fflexcrc || ax25packet->fsmack;

	if (type == PARAM_DATA) {
		ax25_dump(ax25packet, data + 1, length-1-(ax25packet->fcrc ? 2 : 0 ), hexdump);
		return;
	}

	val = data[1];

	switch (type) {
	case PARAM_TXDELAY:
		lprintf(T_KISS, "TX Delay: %lu ms\n", val * 10L);
		break;
	case PARAM_PERSIST:
		lprintf(T_KISS, "Persistence: %u/256\n", val + 1);
		break;
	case PARAM_SLOTTIME:
		lprintf(T_KISS, "Slot time: %lu ms\n", val * 10L);
		break;
	case PARAM_TXTAIL:
		lprintf(T_KISS, "TX Tail time: %lu ms\n", val * 10L);
		break;
	case PARAM_FULLDUP:
		lprintf(T_KISS, "Duplex: %s\n", val == 0 ? "Half" : "Full");
		break;
	case PARAM_HW:
		lprintf(T_KISS, "Hardware %u\n", val);
		break;
	case PARAM_RETURN:
		lprintf(T_KISS, "RETURN\n");
		break;
	default:
		lprintf(T_KISS, "code %u arg %u\n", type, val);
		break;
	}
}




void
dev_dump( struct ifreq *pifr, unsigned char *buffer, int size)
/* device dump */
{
    int prot = -1;
    time_t packettime;

    /* watching loopback is a bad idea, because we use lo ourself ... */
    if( (pifr->ifr_name[0] == 'l') || (pifr->ifr_name[1] == 'o') ) {
	return;
    }

    time(&packettime);
    lprintf(T_TIMESTAMP, "(%s) ", time2str(&packettime) );
    lprintf(T_PROTOCOL, "%s - sa_family:%d\n", pifr->ifr_name,
					       pifr->ifr_hwaddr.sa_family);
    if( pifr->ifr_hwaddr.sa_family == 1 )
	prot = get16(buffer+12);
    else if( pifr->ifr_hwaddr.sa_family == 512 )
	prot = 512;
    else if( pifr->ifr_hwaddr.sa_family == 256 )
	prot = 256;

    switch( prot ) {
	case 256: /* slip ? */
	case 512: /* ppp ? */
		 ip_dump(buffer, size, ASCII);
		 break;
	case ETH_P_IP:
	case ETH_P_ARP:
		lprintf(T_PROTOCOL, "ethernet MAC: " );
		/* ethernet MAC-Adresses */
		lprintf(T_ADDR, "%02x-%02x-%02x-%02x-%02x-%02x",buffer[6],buffer[7],buffer[8],buffer[9],buffer[10],buffer[11]);
		lprintf(T_ZIERRAT, "->");
		lprintf(T_ADDR, "%02x-%02x-%02x-%02x-%02x-%02x",buffer[0],buffer[1],buffer[2],buffer[3],buffer[4],buffer[5]  );

		lprintf(T_PROTOCOL, " Prot.:%04x\n", prot );
		buffer += 14;
		size -= 14;
		switch( prot ) {
		    case ETH_P_IP:
			    ip_dump(buffer, size, ASCII);
			    break;
		    case ETH_P_ARP:
			    arp_dump(buffer, size);
			    break;
		   default:
			   hex_dump(buffer, size);
		}
		break;
	default:
		hex_dump(buffer, size);
		break;
    }
}

