/*
 *  ax25spy - a client for ax25spyd
 *
 *  Copyright (C) 1999 Free Software Foundation, Inc.
 *  Copyright (C) 1999 Walter Koch, dg9ep
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* monix.c - Mainpart of the ax25spy - client program
 * $Id: monix.c,v 1.34 1999/07/13 22:26:11 walter Exp $
 */


#include <config.h>
#include "mondefs.h"
#include <sys/types.h>
#include <sys/socket.h>
#ifdef __GLIBC__
#include <net/if.h>
#else
#include <linux/if.h>
#endif
#include <sys/ioctl.h>
#include <sys/signal.h>
#include <sys/time.h>
#include <syslog.h>
#include <netdb.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <curses.h>
#include <errno.h>



#include "monix.h"
#include "sockets.h"
#if STATICLINK
#include "monixd.h"
#endif


int color = 0;		/* Colorized? */
int sevenbit = 0;	/* Are we on a 7-bit terminal? */
int ibmhack = 0;	/* IBM mapping? */


static int end = 0;
static int ctrlced = 0;
static int sockCmd;
static int fVerbose;
static int fSpy;
static int fHTML = 0;
static int fToFile = 0;
static int fFromFile = 0;
static FILE *fpRawPR;

/* mapping of IBM codepage 437 chars 128-159 to ISO latin1 equivalents
 * (158 and 159 are mapped to space)
 */
unsigned char ibm_map[32] =
{
	199, 252, 233, 226, 228, 224, 229, 231,
	234, 235, 232, 239, 238, 236, 196, 197,
	201, 230, 198, 244, 246, 242, 251, 249,
	255, 214, 220, 162, 163, 165,  32,  32
};


void
printQSO( struct t_qso *pQSO, int fLongTime, int fFrameStat, int fTypeStat)
{
    char buf[200];
    int i;
    char buf1[20];
    char buf2[20];
    char buf3[90];

    if( fLongTime ) {
	/* time2str uses static buffers ... */
	sprintf(buf, "%d: (%s)", pQSO->qsoid, time2str(&pQSO->firstheard) );
	fputs(buf, stdout);
	sprintf(buf, " - (%s)  otherqso:%d\n", time2str(&pQSO->lastheard), pQSO->otherqsoid );
	fputs(buf, stdout);
    }

    ax25call2strBuf( &pQSO->fmcall, buf1);
    ax25call2strBuf( &pQSO->tocall, buf2);
    buf3[0] = 0;
    if( fFrameStat ) {
	sprintf(buf3, "%4d  %7d/%-7d %5.2f%%",
		       pQSO->nFrames,
			    pQSO->nInfoBytes,
				pQSO->nTotalBytes,
		       100 * (float)pQSO->nInfoBytes / pQSO->nTotalBytes
	);
    }
    sprintf(buf, "%3d:%-9s <-> %-9s %s (%s) %s\n",
		  pQSO->qsoid,
		      buf1,    buf2, buf3,
		  time2str(&pQSO->lastheard),
		  pid2str(pQSO->pid)
    );
    fputs(buf, stdout);
    if( fTypeStat ) {
	/* Frametyps */
	for( i=0 ; i<nORD_FRAMETYP ; i++ )
		if( pQSO->frametype[i] > 0 ) {
			sprintf(buf, "  %s:%d", ord2frametypestring(i), pQSO->frametype[i] );
			fputs(buf, stdout);
		}
	sprintf(buf, "\n");
	fputs(buf, stdout);
    }
    fflush(stdout);
    return;
}



void
tcprint(int dtype, char *str, int size)
/*  printf in Technicolor (TM) (available in selected theatres only)
 *  dtype contains the type, transmitted by ax25spyd
 */
{
	static bold=0;
	unsigned char *p;
	chtype ch;
	int i, total;
	/*---------- RAWPACKET----------------------------------------------*/
	if( dtype == T_RAWPACKET ) {
	    if( fToFile ) {
		fwrite(&size, sizeof(size), 1, fpRawPR);
		fwrite(str,   size,	    1, fpRawPR);
	    }
	    return;
	}
	/*---------- MHeard ------------------------------------------------*/
	if( dtype == T_MHEARDSTRUCT ) {
	    int i;
	    char buf[200];
	    struct t_mheard *pmheard = (struct t_mheard*)str;

	    if( pmheard->version != sizeof(struct t_mheard) ) {
		fprintf(stderr,"\nax25spy(d)-version error, t_mheard-versions do not match\n");
		exit(1);
	    }

	    fprintf(stdout, "\n--- Frames ---\n");
	    /* Frametyps */
	    total = 0;
	    for( i=0 ; i<nORD_FRAMETYP ; i++ )
		if( pmheard->frametype[i] > 0 ) {
		    sprintf(buf, "%s:%d  ", ord2frametypestring(i), pmheard->frametype[i] );
		    total += pmheard->frametype[i];
		    fputs(buf, stdout);
		}
	    sprintf(buf, "- total:%d\n",total);
	    fputs(buf, stdout);
	    /* PIDs */
	    total = 0;
	    for( i=0 ; i<256 ; i++ )
		if( pmheard->pid[i] > 0 ) {
		    sprintf(buf, "%s:%d ", pid2str(i), pmheard->pid[i] );
		    total += pmheard->pid[i];
		    fputs(buf, stdout);
		}
	    sprintf(buf, "- total:%d\n",total);
	    fputs(buf, stdout);
	    /* others */
	    sprintf(buf,"ICMP:%d  UDP:%d  TCP:%d\nDX-Clust:%d  DAMA:%d\n",
			 pmheard->protICMP,pmheard->protUDP,pmheard->protTCP,pmheard->nDXClust, pmheard->damaframes);
	    fputs(buf, stdout);

	    fprintf(stdout, "--- Byte ---\n");
	    sprintf(buf, "payload:%d  total:%d - ratio:%.2f%%\n",
		      pmheard->nInfoBytes,
				    pmheard->nTotalBytes,
		     100 * (float)pmheard->nInfoBytes / pmheard->nTotalBytes
	    );
	    fputs(buf, stdout);
	    fflush(stdout);
	    return;
	}

	/*---------- Call-MHeard -------------------------------------------*/
	if( dtype == T_CALLMHEARDSTRUCT ) {
	    char buf[300];
	    struct t_CallMheard *pmheard = (struct t_CallMheard*)str;

	    if( pmheard->version != sizeof(struct t_CallMheard) ) {
		fprintf(stderr,"\nax25spy(d)-version error, t_CallMheard-versions do not match\n");
		exit(1);
	    }

	    if( fVerbose ) {
		sprintf(buf, "(%s)",      time2str(&pmheard->firstheard) );
		fputs(buf, stdout);
		sprintf(buf, " - (%s)\n", time2str(&pmheard->lastheard ) );
		fputs(buf, stdout);
	    }
	    sprintf(buf, "%-9s  %4d  %7d/%-7d %5.2f%%   (%s)  ",
			   ax25call2str(&pmheard->call),
			   pmheard->nFrames,
			   pmheard->nInfoBytes,
			   pmheard->nTotalBytes,
		     100 * (float)pmheard->nInfoBytes / pmheard->nTotalBytes,
			   time2str(&pmheard->lastheard)
	    );
	    fputs(buf, stdout);
	    for( i=0 ; i<256 ; i++ )
		if( pmheard->nPID[i] > 0 ) {
		    sprintf(buf, "%s:%d  ", pid2str(i), pmheard->nPID[i] );
		    fputs(buf, stdout);
		}
	    sprintf(buf, "\n");
	    fputs(buf, stdout);
	    if( fVerbose ) {
		/* Frametyps */
		for( i=0 ; i<nORD_FRAMETYP ; i++ )
		    if( pmheard->frametype[i] > 0 ) {
			sprintf(buf, "  %s:%d", ord2frametypestring(i), pmheard->frametype[i] );
			fputs(buf, stdout);
		    }
		sprintf(buf, "\n");
		fputs(buf, stdout);
	    }
	    fflush(stdout);
	    return;
	}

	/*------- QSO-Mheard -------------------------------*/
	if( dtype == T_QSOMHEARDSTRUCT ) {
	    struct t_qso *pQSO = (struct t_qso*)str;

	    if( pQSO->nFrames )
		printQSO( pQSO, fVerbose, fVerbose, fVerbose );
	    return;
	}
	/*----------- Neuer Spy----------------------------------------*/
	if( (dtype == T_SPYHEADER) && fSpy ) {
	    struct t_qso *pQSO = (struct t_qso*)str;
	    fputs("\n*** Spy started:", stdout); fflush(stdout);
	    printQSO( pQSO, 1,1,1 /*1,1,1=verbose*/ );
	    return;
	}
	/*-----------SPYDATA------------------------------------------*/
	if( (dtype == T_SPYDATA) && fSpy ) {
	    char buf[300];
	    convertBufferToReadable( str+sizeof(t_qsoid), size-sizeof(t_qsoid) );
	    sprintf(buf,"\nQSO %d:\n%s", (t_qsoid)*str,str+sizeof(t_qsoid) );
	    fputs(buf,stdout); fflush(stdout);
	    return;
	}
	/*-------------------New QSOs, but no Spyng--------------------------*/
	if( dtype == T_OFFERQSO ) {
	    struct t_qso *pQSO = (struct t_qso*)str;
	    char buf[200];
	    char buf1[20];
	    char buf2[20];

	    ax25call2strBuf( &pQSO->fmcall, buf1);
	    ax25call2strBuf( &pQSO->tocall, buf2);
	    sprintf(buf, "%-9s <-> %-9s (%s)  qsoids: %d <-> %d\n",
			  buf1,    buf2, time2str(&pQSO->firstheard),
			  pQSO->qsoid, pQSO->otherqsoid );
	    fputs(buf, stdout);
	    fflush(stdout);
	    return;
	}

	/*-------------------------------------------------------*/
	if( dtype == T_VERSION ) {
	    char buf[200];
	    str[size] = '\0';
	    sprintf(buf, "daemon found: %s\n\r", str);
	    fputs(buf, stdout);
	    fflush(stdout);
	    return;
	}
	if( dtype == T_PROTVERSION ) {
	    int *pi = (int*)str;
	    char buf[200];
	    sprintf(buf, "protocol version %d\n\r", *pi);
	    fputs(buf, stdout);
	    fflush(stdout);
	    return;
	}
	/*------- Everything else -------------------------------*/
	if( fHTML ) {
	    if( (dtype == T_DATA) && bold ) {
		fputs("</b>", stdout);
		bold = 0;
	    } else if( (dtype != T_DATA) && (bold == 0) )  {
		fputs("<b>", stdout);
		bold++;
	    };
	}

	if (color) {
		for (p = str, i = 0; i < size; p++, i++) {
			ch = *p;
			if (sevenbit && ch > 127)
				ch = '.';
			if ((ch > 127 && ch < 160) && ibmhack)
				ch = ibm_map[ch - 128] | A_BOLD;
			else if ((ch < 32) && (ch != '\n'))
				ch = (ch + 64) | A_REVERSE; /* ctrlch inversen */

			if ((dtype == T_ADDR   ) || (dtype == T_PROTOCOL)
			 || (dtype == T_AXHDR  ) || (dtype == T_IPHDR	)
			 || (dtype == T_ROSEHDR))
			    ch |= A_BOLD; /* header fett */
			ch |= COLOR_PAIR(dtype);
			addch(ch);
		}
	} else {
		if( dtype != T_DXCLUST )
		    for (p = str, i = 0; i < size; p++, i++)
			    if ( (*p < 32  && *p != '\n')
			      || (*p > 126 && *p < 160 && sevenbit)
				)
				    *p = '.';
		fputs(str, stdout);
		if( dtype == T_DXCLUST )
		    fputs("\n", stdout);
		fflush(stdout);
	}
}

int initcolor(void)
{
	if (!has_colors)
		return 0;
	initscr();		/* Start ncurses */
	start_color();		/* Initialize color support */
	refresh();		/* Clear screen */
	noecho();		/* Don't echo */
	wattrset(stdscr, 0);	/* Clear attributes */
	scrollok(stdscr, TRUE); /* Like a scrolling Stone... */
	leaveok(stdscr, TRUE);	/* Cursor position doesn't really matter */
	idlok(stdscr, TRUE);	/* Use hardware ins/del of the terminal */
	nodelay(stdscr, TRUE);	/* Make getch() nonblocking */

	/* Pick colors for each type */
	init_pair(T_ZIERRAT,   COLOR_WHITE,   COLOR_BLACK);

	init_pair(T_TIMESTAMP, COLOR_MAGENTA, COLOR_WHITE);
	init_pair(T_PORT,      COLOR_BLACK,   COLOR_WHITE);

	init_pair(T_DATA,      COLOR_WHITE,   COLOR_BLACK);
	init_pair(T_ERROR,     COLOR_RED,     COLOR_BLACK);
	init_pair(T_PROTOCOL,  COLOR_CYAN,    COLOR_BLUE);
	init_pair(T_AXHDR,     COLOR_WHITE,   COLOR_BLUE);
	init_pair(T_IPHDR,     COLOR_WHITE,   COLOR_BLACK);
	init_pair(T_ADDR,      COLOR_GREEN,   COLOR_BLACK);
	init_pair(T_ROSEHDR,   COLOR_WHITE,   COLOR_BLACK);
	init_pair(T_KISS,      COLOR_MAGENTA, COLOR_BLACK);
	init_pair(T_BPQ,       COLOR_MAGENTA, COLOR_BLACK);
	init_pair(T_TCPHDR,    COLOR_BLUE,    COLOR_BLACK);

	init_pair(T_CALL,      COLOR_GREEN,   COLOR_BLUE);
	init_pair(T_FMCALL,    COLOR_GREEN,   COLOR_BLUE);
	init_pair(T_TOCALL,    COLOR_GREEN,   COLOR_BLUE);
	init_pair(T_VIACALL,   COLOR_BLACK,   COLOR_BLUE);

	init_pair(T_HDRVAL,    COLOR_CYAN,   COLOR_BLACK);

	init_pair(T_MHEARD,    COLOR_GREEN,  COLOR_BLACK);

	return 1;
}



void
quit_handler(int dummy)
{
    end = 1;
    ctrlced = 1;
    /* syslog(LOG_DEBUG, "terminating on SIG %d",dummy); */
}


void
sendCmd( char *fmt, ...)
{
    va_list args;
    char sCmd[1024];
    struct t_protHeader protHeader;

    va_start(args, fmt);
    vsnprintf(sCmd, 1024, fmt, args);
    va_end(args);

    protHeader.version = 1;
    protHeader.dsize   = strlen(sCmd) ;
    protHeader.dtype   = TT_CMD_FOLLOWS;
    write( sockCmd, &protHeader, sizeof(protHeader) ) ;
    write( sockCmd, sCmd, strlen(sCmd) );
}


void
printVersion(void)
{
    printf("ax25spy %s - compiled: %s %s\n\r", VERSION,__DATE__,__TIME__);
}


int
main(int argc, char **argv)
{
#define BUFSIZE 	4500
    unsigned char buffer[BUFSIZE];
    int fTimestamp = 0;
    int dxclusterspy = 0;
    int size;
    char *sfilter = NULL;
    char *sspyid = NULL;
    int c;
    int fMHeard = 0;
    int fCallMHeard = 0;
    int fQSOMHeard   = 0;
    int fOnlyIFrames = 0;
    int fShowNewQSOs = 0;
    int fShowAllNewQSOs = 0;
    int fOnlyIP      = 0;
    int fOnlyHeaders = 0;
    int fQuiet = 0;

    openlog("ax25spy", LOG_PID, LOG_USER);

    color = 1;

    while( (c = getopt(argc, argv, "bcdfF:HiImnNpqQrsS:tTvV")) != -1 ) {
	    switch( c ) {
		    case 'b':
			    color = 0;
			    break;
		    case 'c':
			    fCallMHeard++;
			    color = 0;
			    break;
		    case 'd':
			    dxclusterspy = 1;
			    color = 0;
			    break;
		    case 'f':
			    fToFile = 1;
			    break;
		    case 'F':
			    sfilter = optarg;
			    break;
		    case 'H':
			    fHTML++ ;
			    break;
		    case 'i':
			    ibmhack = 1;
			    break;
		    case 'I':
			    fOnlyIFrames = 1;
			    break;
		    case 'm':
			    fMHeard++;
			    color = 0;
			    break;
		    case 'n':
			    fShowAllNewQSOs++;
			    color = 0;
			    break;
		    case 'N':
			    fShowNewQSOs++;
			    color = 0;
			    break;
		    case 'p':
			    fOnlyIP = 1;
			    break;
		    case 'q':
			    fQSOMHeard++;
			    color = 0;
			    break;
		    case 'r':
			    fFromFile = 1;
			    break;
		    case 'Q':
			    fQuiet++;
			    break;
		    case 's':
			    fSpy++;
			    color = 0;
			    break;
		    case 'S':
			    sspyid = optarg;
			    color = 0;
			    break;
		    case 't':
			    fTimestamp = 1;
			    break;
		    case 'T':
			    fOnlyHeaders = 1;
			    break;
		    case 'v':
			    fVerbose++;
			    break;
		    case 'V':
			    printVersion();
			    printf("\nThis program is free software; you can redistribute it and/or modify\n"
				   "it under the terms of the GNU General Public License as published by\n"
				   "the Free Software Foundation; either version 2 of the License, or\n"
				   "(at your option) any later version.\n\n"
				   "This program is distributed in the hope that it will be useful,\n"
				   "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
				   "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\n");
			    return 0;
		    case '?':
			    fprintf(stderr, "Usage: %s [-options]\n see  man ax25spy\n",argv[0]);
			    return 1;
	    }
    }

    signal(SIGINT, quit_handler);

    if( color ) {
	color = initcolor();	/* Initialize color support */
	if (!color) {
		printf("Could not initialize color support.\n");
		return 1;
	}
    }

    if( !fQuiet ) {
	printVersion();
	if( color ) refresh();
    }

#if STATICLINK
    initDaemon();
#endif
    /* Verbindung zum Server aufnehmen */
    sockCmd = OpenClientSocket("127.0.0.1",MONIX_PORT);
    if(sockCmd == -1) {
	printf("cannot create ax25spy client socket\n");
	if( color ) endwin();
	exit(1);
    }


    if( fFromFile ) {
	if( (fpRawPR = fopen(AX25VARDIR"/ax25spy", "r")) == NULL) {
	    char buf[100];
	    sprintf(buf, "cannot open "AX25VARDIR"/ax25spy: %d, %s\n",errno,strerror(errno));
	    syslog(LOG_ERR, buf);
	    fputs(buf, stderr);
	    if( color ) endwin();
	    exit(1); /* Naja */
	}
	sendCmd("RAWPACKET");
	/* RAWDATA <size> <sa-data> <pr-data> */
	/* fRead */
	exit(1);
    }


    /* issue command */
    if( dxclusterspy ) {
	sendCmd("DXCLUSTERSPY");
    } else if( fMHeard ) {
	sendCmd("MHEARD");
    } else if( sspyid ) {
	fSpy = 1; /* brauchen wir noch wg. Ausgabe */
	sendCmd("AUTOSPYOTHER");
	sendCmd("SPYID %s",sspyid);
    } else if( fSpy ) {
	if( sfilter ) {
	    sendCmd("SPYINCL %s",sfilter);
	} else {
	    printf("-s requires -F, aborting\n");
	    if( color ) endwin();
	    exit(1);
	}
    } else if( fCallMHeard ) {
	if( sfilter ) { sendCmd("CALLMHEARD %s",sfilter); }
		 else { sendCmd("CALLMHEARD"); }
    } else if( fShowNewQSOs ) {
	/* first all QSOs weve got */
	sendCmd("OFFERALLQSO" );
	/* and from now on, all new QSOs */
	sendCmd("OFFERNEWQSO" );
    } else if( fShowAllNewQSOs ) {
	/* first all QSOs weve got */
	sendCmd("OFFERALLQSO" );
	/* and from now on, all new QSOs */
	sendCmd("OFFERALLNEWQSO" );
    } else if( fQSOMHeard   ) {
	if( sfilter ) { sendCmd("QSOMHEARD %s",sfilter); }
		 else { sendCmd("QSOMHEARD"); }
    } else {
	sendCmd("STARTMONI");
	if( sfilter ) { sendCmd("HEADERFILTER %s",sfilter); }
    }

    if( fToFile ) {
	if( (fpRawPR = fopen(AX25VARDIR"/ax25spy", "w")) == NULL) {
	    char buf[100];
	    sprintf(buf, "cannot open "AX25VARDIR"/ax25spy: %d, %s\n",errno,strerror(errno));
	    syslog(LOG_ERR, buf);
	    fputs(buf, stderr);
	    if( color ) endwin();
	    exit(1); /* Naja */
	}
	sendCmd("RAWPACKET");
    }
    if( fTimestamp   ) { sendCmd("TIMESTAMP YES"); }
    if( fOnlyIFrames ) { sendCmd("ONLYIFRAMES"  ); }
    if( fOnlyIP      ) { sendCmd("ONLYIP"       ); }
    if( fOnlyHeaders ) { sendCmd("ONLYHEADERS"  ); }
    sendCmd("VERSION");
    sendCmd("PROTVERSION");

    for (;end==0;) {
	fd_set fdReadset;
	struct timeval tv;
	int res;
	struct t_protHeader protHeader;

	/* prepare select() */
	FD_ZERO(&fdReadset);
	FD_SET(sockCmd,&fdReadset);
	tv.tv_sec=1; /* wait 1 second (?) */
	tv.tv_usec=0;
	res = select( sockCmd+1, &fdReadset, 0, 0, &tv );
	if( res == -1 )
	    end = 1;
	else if( FD_ISSET(sockCmd, &fdReadset) ) {
	    /* read header */
	    size = readfull(sockCmd,&protHeader,sizeof(protHeader));
	    /* syslog(LOG_DEBUG, "read dtype %d   dsize:%d",protHeader.dtype,protHeader.dsize); */
	    if( size <= 0 ) {
		syslog(LOG_DEBUG, "aborting, because reading protheader returned %d (%s)",
				   size,
				   (size==-1 ? strerror(errno):"ax25spyd down?") );
		end = 1;
	    }
	    /* syslog(LOG_DEBUG, "type: %d   size: %d",protHeader.dtype, protHeader.dsize ); */
	    if( !end ) {
		if( protHeader.dtype == T_ENDOFDATA ) {
		    end = 2;
		}
		if( protHeader.dsize > 0 ) {
		    /* and now read the actual data */
		    if( (size = readfull(sockCmd, buffer, protHeader.dsize)) <= 0) {
			syslog(LOG_DEBUG, "ending, because reading data returned %d (%s)",
					   size,
					   (size==-1 ? strerror(errno):"ax25spyd down?") );
			end = 1;
		    } else {
			buffer[size] = '\0';
			tcprint(protHeader.dtype, buffer, size);
			if( color ) refresh();
		     }
		}
	    }
	    if( fMHeard ) {
		end = 2;
		/* Or, for endless looping:  sleep(5); sendCmd("MHEARD"); */
	    }
	}
    } /* for() */
    if( color )
	endwin();
    if( end == 1 ) {
	if( ctrlced )
	    printf("\nabort due ctrl-c - shutting down...\n");
	else
	    printf("\nconnection to ax25spyd lost - shutting down...\n");
    }
    if( fToFile )
	fclose(fpRawPR);
    close(sockCmd);
    if( end == 1 )
	printf("\nax25spy terminated\n");
    return(0);
}

